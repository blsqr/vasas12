#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''This file holds the Universe of the Vasas 2012 paper.'''

__version__ 	= "0.1 -- not maintained"

from deeevolab import get_log_level
from deeevolab.logging import setup_logger
from deeevolab.config import get_config
from deeevolab.tools import purge, try_and_except
from deeevolab.universe import Universe
from deeevolab.acn.compartment import Compartment

# Initialise loggers and config file
log 	= setup_logger(__name__, level=get_log_level(is_universe=True))
cfg 	= get_config(__name__, is_project_module=True)
log.debug("Loaded module, logger, and config.")
# -----------------------------------------------------------------------------

class Vasas12(Universe):
	'''The Universe is where everything takes place. It is a child of the ParentUniverse class and provides an interface for management of the simulation, plotting of data and saving of data.

	It is special in the sense that it takes care of everything specific regarding the setup and simulation of the Vasas12 paper.
	'''

	def __init__(self, *args, num_compartments: int, compartment_init: dict, compartment_setup: dict, **kwargs):

		# Initialise the parent universe
		super().__init__(*args, **kwargs)

		# Initialise attributes
		self.compartments 		= []
		self._static_comps		= []
		self._exploding_comps 	= []

		# Create and setup compartments
		self.init_compartments(num_compartments,
		                       init_kwargs=compartment_init)

		self.setup_compartments(setup_kwargs=compartment_setup)

		# Initialise monitors here, if needed
		self.setup_monitors(**cfg.universe_monitors)

		# Save the initialisation parameters as metadata
		# args and kwargs are saved in ParentUniverse
		log.debug("Writing metadata ...")
		self.metadata.update_item('init_kwargs',
		                          dict(num_compartments=num_compartments,
		                               compartment_init=compartment_init,
		                               compartment_setup=compartment_setup))

		# Use the method implemented in ParentUniverse to finish initialisation
		self._init_finished()

		return

	def init_compartments(self, num_comps: int, *args, init_kwargs: dict) -> None:
		'''Add compartments to the universe object.

		Args:
			num_compartments (int) : number of compartments to add
			comp_init_kwargs (dict) : common kwargs to initialise the Compartment objects

		Returns:
			None
		'''
		log.info("Adding %d compartment(s) to the universe...", num_comps)

		if num_comps > 1:
			log.warning("Having more than one compartment might cause errors when saving monitors or data! TO BE FIXED!") # FIXME

		num_already_present 	= len(self.compartments)
		target_num_comps		= num_already_present + num_comps

		for n in range(num_already_present, target_num_comps):

			name = cfg.strf.comp_name.format(comp_no=n,
			                                 digits=max(2, len(str(target_num_comps))))

			# Initialize compartment ...
			_comp 	=	Compartment(name=name,
			                     	max_num_steps=self.max_num_steps,
			                     	dirs=self.dirs,
			                     	data_saver=self.data_saver,
			                     	**init_kwargs)

			self.compartments.append(_comp)

		# Add the compartments to the list of monitor containers
		self.add_monitor_containers(*self.compartments)

		return

	def setup_compartments(self, *args, setup_kwargs: dict):
		''' Setting up compartments with molecules'''
		log.info("Setting up molecules and reactions in compartments ...")
		for comp in self.compartments:
			comp.setup_compartment(**setup_kwargs)

	# Simulation ..............................................................

	@try_and_except()
	def run_simulation_step(self, num_steps_chem: int, tau: float) -> bool:
		'''Runs one step of the network simulation. Is called by run_simulation(), which is implemented in the parent Universe.

		Args:
			current_step (int) : current step number in run_simulation()
			num_steps (int) : number of steps to simulate the chemical dynamics
		'''

		# loop over compartments
		for comp in self.compartments:
			# skip static compartments
			if comp in self._static_comps or comp in self._exploding_comps:
				log.debug("Skipping compartment {comp:name}, because it was marked static or exploding.".format(comp=comp))
				continue

			# Increment the compartment's step count
			comp.increment_steps()

			# Create new reactions, if molecules got above threshold
			if comp.new_mols:
				comp.update_reactions()

			# Propagate circles from outer to inner
			comp.propagate_circles()

			# Run chemical dynamics and propagate molecules
			comp.perform_chemical_dynamics(num_steps_chem, tau=tau)

			# Update the candidate molecules and store whether new ones emerged
			comp.new_mols 	= comp.update_candidates()

			# Analyse mid-step
			self.run_analysis(cfg.analysis.get('mid_step'))

			# Build the network, if new molecules were added
			comp.update_network(properties_only=not comp.new_mols)

			# Give out some info, if new molecules were created and/or network updated
			if comp.new_mols:
				comp.log_state_info()

		# Everything ok, return True
		return True

	# Analysis ................................................................
	# Analysis methods .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .

	def _check_static(self) -> list:
		''' Checks, whether compartments are static ...'''
		log.debug("Checking if compartments are static ...")

		for comp in self.compartments:
			if comp.is_static():
				log.state("Compartment {:name} is static.".format(comp))
				self._static_comps.append(comp)

		return self._static_comps

	def _check_exploding(self, *args, explosion_kwargs: dict) -> list:
		''' Checks, whether the compartments are 'exploding', i.e. whether they are very quickly growing... '''
		log.debug("Checking if compartments are exploding ... ")

		for comp in self.compartments:
			if comp.is_exploding(**explosion_kwargs):
				log.state("Compartment {:name} is exploding.".format(comp))
				self._exploding_comps.append(comp)

		return self._exploding_comps

	# ...and for automated analysis .  .  .  .  .  .  .  .  .  .  .  .  .  .
	# NOTE have to start with _ana_

	def _ana_plot_compartment_networks(self, tmp_pos_mode: str=None, output: str=None, output_strf: str=None, cleanupdir_strf: str=None, **kwargs):
		''' Plots all networks.

		Args:
			tmp_pos_mode (str, None) : if 'read', reads it from the network attribute; if 'save', saves it to the network; if 'both', does both.
		'''
		log.note("Plotting ReactionNetworks of %d compartments ...", len(self.compartments))

		for comp in self.compartments:
			# Format strf with n_cyc information
			# This removes one layer of {} brackets from the format string
			# NOTE possible to add some behaviour here
			if isinstance(output_strf, str):
				_output_strf = output_strf.format()
			else:
				_output_strf = output_strf

			comp.network.plot(output=output,
			                  output_strf=_output_strf,
			                  tmp_pos_mode=tmp_pos_mode,
			                  **kwargs)

			# Cleanup
			if isinstance(cleanupdir_strf, str):
				purge_dir 	= cleanupdir_strf.format(comp=comp)
				purge(purge_dir, 'cbar.tmp.pdf')
				log.debug("Removed temporary files from '%s'.", purge_dir)

		return

	def _ana_plot_network_cycles(self, *_, mode: str, output: str=None, output_strf: str=None, cleanupdir_strf: str=None, length_limit: int=None, max_num_plots: int=None, **kwargs):
		''' Plots the detected cycles of the compartments' network.'''
		log.note("Plotting cycles of %d compartments ...", len(self.compartments))

		for comp in self.compartments:

			num_plots 	= 0

			for n_cyc, cyc in enumerate(comp.network.cycles[mode]):
				if length_limit is not None and len(cyc) > length_limit:
					log.debugv("Skipping cycle #%d with length %d > %d.",
					          n_cyc, len(cyc), length_limit)
					continue

				log.debugv("Plotting cycle #%d with length %d ...",
				          n_cyc, len(cyc))

				# Format strf with n_cyc information
				# This removes one layer of {} brackets from the format string
				if isinstance(output_strf, str):
					_output_strf = output_strf.format(n_cyc=n_cyc,
					                                 cyc_len=len(cyc))
				else:
					_output_strf = output_strf

				comp.network.plot(output=output,
				                  output_strf=_output_strf,
				                  subgraph=cyc,
				                  **kwargs)

				num_plots += 1
				if max_num_plots and num_plots >= max_num_plots:
					log.debug("max_num_plots %d reached.", max_num_plots)
					break

			# Cleanup
			if isinstance(cleanupdir_strf, str):
				purge_dir 	= cleanupdir_strf.format(comp=comp)
				purge(purge_dir, 'cbar.tmp.pdf')
				log.debug("Removed temporary files from '%s'.", purge_dir)

		return

	# Saving ..................................................................

	def cleanup_and_save(self):
		''' '''
		super().cleanup_and_save()

		log.note("Saving networks ...")
		filestrf 	= cfg.strf.network_file
		for comp in self.compartments:
			if not hasattr(comp, 'network') or comp.network is None:
				log.debug("Skipping saving of network for compartment {comp:name}, because it has no network.".format(comp=comp))
				continue

			path 	= comp.dirs['data']
			fname 	= filestrf.format(comp=comp, nw=comp.network)
			comp.network.save(path + fname)

	# Stop Conditions .........................................................

	def _sc_is_static(self):
		self._check_static()

		if len(self._static_comps) >= len(self.compartments):
			log.highlight("All compartments are static.")
			return True, 'is_static'
		return False, None

	def _sc_exploding(self, **explosion_kwargs): # TODO
		self._check_exploding(explosion_kwargs=explosion_kwargs)

		if len(self._exploding_comps) >= len(self.compartments):
			log.highlight("All compartments are exploding.")
			return True, 'exploding'
		return False, None

	# Helper methods ..........................................................
	def _increment_steps(self):
		''' Increment the step size of the universe and all Environment-derived objects, that are affected by this class's run_simulation_step method, i.e. the compartments.
		'''
		super().increment_steps() # increment those of the universe itself

		# Also increment those of the compartments
		for comp in self.compartments:
			comp.increment_steps()

		return

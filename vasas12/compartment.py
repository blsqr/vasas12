#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''This file holds the environment-related class Compartment.
Currently, the approach taken here is resembling an approach taken by Vasas et al., 2012.'''

# Internal
import itertools
from collections import OrderedDict

# External
import numpy as np

# Local Packages
from deeevolab.logging import setup_logger
from deeevolab.config import get_config
from deeevolab.evo_units import Environment


# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Cython chemistry or python chemistry?
if cfg.use_cython_chemistry:
	from .cy_chemistry import Molecule, Condensation, Cleavage
	log.debug("Loaded cython chemistry module.")
else:
	from .chemistry import Molecule, Condensation, Cleavage
	log.debug("Loaded regular (python) chemistry module.")

# Some constants
DEBUG 				= False
R_STRF_FWD 			= cfg.rxn_strf.fwd
R_STRF_REV 			= cfg.rxn_strf.rev

CYC_FMODES_MAP 		= {
	# a mapping of cycle detection filter modes to shorter names (for display)
	'all' 			: 'total',
	'catalytic' 	: 'cat',
	'autocatalytic' : 'acc',
	'above_thrs' 	: '>thrs',
	'viable_cores'  : 'viable'
}


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class Compartment(Environment):
	'''A compartment object has several different tasks:
	1) Keep track of the species of Molecules inside it
	2) Manage regular and catalysed reactions of species inside it
	3) Identify and access the catalytic network
	...
	# TODO finish writing docstring
	'''

	# TODO finish writing docstring
	def __init__(self, *env_args, constants: dict, monitor_init: dict=cfg.monitor_init, **env_kwargs) -> None:
		'''Initialises the compartment with initial rates and the firing disk populated with initial concentration

		Args:
			env_args : passed to parent class Environment

			constants (dict) : compartment constants

			monitor_init (dict) : initialisation parameters for monitors

			env_kwargs : passed to parent class Environment

			monitor_kwargs (dict) : which monitors to initialise

		Returns:
			None
		'''

		super().__init__(*env_args, **env_kwargs)

		# Set attributes . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Compartment constants
		self.C_THRS			= constants['c_thrs']
		self.SKIP_LENGTH	= constants['skip_length']
		self.RATES 			= constants['rates']
		self.MOLECULE_INIT	= constants['molecule_init']
		self.MAX_PROB_SKIPS	= constants['max_prob_skips']
		self.RXN_PROB 		= constants['rxn_prob']

		#Iinitialise empty dicts to keep track of molecules and catalysts
		self.all 			= OrderedDict() # All species ever created
		self.inner 			= OrderedDict() # Old species
		self.outer 			= OrderedDict() # New species
		self.temp 			= OrderedDict() # Newly created species
		# dictionary key is structure string

		self.new_mols 		= True # Flag, whether molecules got above threshold

		# Dicts to keep track of reactions
		self.condensations 	= OrderedDict() # Condensation reactions
		self.cleavages		= OrderedDict() # Cleavage reactions
		# dictionary key is reaction string

		# Reaction network
		if cfg.network_enabled:
			# Import the network library. We do this here and not at the top of the file, because this makes it thread-safe, because it is imported multiple times in every forked process...
			try:
				from .network import ReactionNetwork
				log.debug("Loaded ReactionNetwork.")
			except ImportError:
				log.warning("Could not load ReactionNetwork. Continuing without...")
				self.network 	= None
			else:
				self.network	= ReactionNetwork(comp=self)
		else:
			self.network 		= None

		# Add monitors . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		self.setup_monitors(max_length=self.max_num_steps, **monitor_init)

		# done here. Do not directly get into the main loop, let that be started in a more controlled way and at a later point in time
		log.note("Initialised compartment, network, and monitors.")

		return

	def setup_compartment(self, *args, food_cats: bool, fd_radius: int, food_len: int, literals: list, food_init: dict, c_inicat: float=0.0):
		''' Start setting up molecules. This is called after initialisation.
		'''

		# initialise firing disk molecules (are put into inner circle)
		self.init_firing_disk(radius=fd_radius, food_len=food_len,
		                      literals=literals, food_init=food_init)

		self.log_state_info()

		self.init_reactions(skip_length=fd_radius,
		                    food_cats=food_cats, c_inicat=c_inicat)

		# build initial network
		self.update_network()

		# log some info
		self.log_state_info()

		log.note("Setup compartment.")

		return

	def init_firing_disk(self, *args, radius: int, food_len: int, literals: list, food_init: dict) -> None:
		'''Initialises all possible molecules within a radius and an initial concentration and put them into the inner circle.
		IMPORTANT: Should only be called by __init__

		Args:
			radius (int) : 		the radius to initialise molecules in. For radius n, all possible molecules with a structure length l <= n are initialised.

			food_len (int) : 	until which length molecules (created here) are considered food molecules

			literals (list) :  	which literals the molecules will be composed of

			food_init (dict) : 	the initialisation arguments for the food molecules

		Returns:
			None
		'''

		log.debug("Initialising firing disk with radius %d, food_len %d, literals %s, food_init %s ...", radius, food_len, literals, food_init)

		if not len(literals):
			raise ValueError("No literals were given, argument was '{}'! Cannot initialise firing disk.".format(literals))

		# Create all firing disk molecules with a structure length up to the firing disk radius and the initial concentration
		for s_len in range(1, radius+1):
			for prod in itertools.product(''.join(literals), repeat=s_len):
				# build the structure by joining the literals
				struct 	= ''.join(prod)

				# create the molecule
				if s_len <= food_len:
					# is food
					mol 	= Molecule(struct, **food_init)
				else:
					# not food
					mol 	= Molecule(struct, **self.MOLECULE_INIT)

				# add to dict of all molecules and to inner circle (reference!)
				self.all[struct] 	= mol
				self.inner[struct] 	= mol

		log.note("Created %d molecules with L <= %d, food length <= %d.", len(self.all), radius, food_len)

		return

	def init_reactions(self, *args, skip_length: int, food_cats: bool, c_inicat: float=0.0):
		'''initialise condensation products and reactions'''
		if food_cats:
			# straight-forward: assuming catalysts within the food set, just create the reactions as per the normal procedure (catalyst required)
			log.note("Creating condensations of food molecules ...")

			self._create_condensations('inner', 'inner', 'inner',
			                           skip_length=skip_length,
			                           target='inner')

		else:
			# there are no catalysts in the food set
			log.note("Creating possible condensation products ...")

			# first create the possible condensation products, among which there might be catalysts
			self._create_condensation_products('inner', 'inner',
			                                   skip_length=skip_length,
			                                   target='outer')

			# now set the concentration of the catalysts
			if c_inicat == 0.0:
				log.warning("Concentration of initial catalysts set to 0.0 -- compartment might remain static.")
			self._set_concentration('outer', c_inicat, cats_only=True)
			log.debug("Set concentration of catalysts")

			# Add the possible reactions, catalysed by the newly formed catalysts in the outer region
			log.note("Creating condensations of food and new molecules ...")

			self._create_condensations('inner', 'inner', 'outer',
			                           skip_length=skip_length)

			self._create_condensations('inner', 'outer', 'outer')

			self._create_condensations('outer', 'inner', 'outer')

			# Done. Propagate.
			self.propagate_circles()

		return


	# .........................................................................
	# Used in simulation step (in this order)

	def update_reactions(self) -> None:
		''' '''

		log.info("Updating reactions...")

		num_cnds 	= len(self.condensations)
		num_clvs 	= len(self.cleavages)

		# Old species reside in self.inner
		# New species reside in self.outer
		# Species resulting from new reactions are put into self.temp

		# Assign condensations between one old and one new species and between two new species, catalysed by old or new species
		self._create_condensations('outer', 'outer', 'outer')
		self._create_condensations('outer', 'outer', 'inner')

		self._create_condensations('outer', 'inner', 'outer')
		self._create_condensations('inner', 'outer', 'outer')

		self._create_condensations('outer', 'inner', 'inner')
		self._create_condensations('inner', 'outer', 'inner')

		# Assign cleavages of new species, catalysed by old or new species
		self._create_cleavages('outer', 'outer')
		self._create_cleavages('outer', 'inner')

		# Assign condensations of old species, catalysed by new species
		self._create_condensations('inner', 'inner', 'outer')

		# Assign cleavages of old species, catalysed by new species
		self._create_cleavages('inner', 'outer')

		log.state("New reactions:      %d condensations,  %d cleavages",
		          len(self.condensations) - num_cnds,
		          len(self.cleavages) - num_clvs)

		return

	def propagate_circles(self) -> None:
		'''Moves the content of the outer circle to the inner circle. Then moves the concent of the temporary circle to the outer circle.'''

		# Move outer circle to inner
		self.inner.update(self.outer)
		self.outer.clear()

		# Move temp circle to outer
		self.outer.update(self.temp)
		self.temp.clear()

		return

	def perform_chemical_dynamics(self, num_steps: int, tau: float=None) -> None:
		'''Performs the chemical dynamics within the compartment by calling all reactions

		Args:
			num_steps (int) 		: number of steps to perform
			tau (float, optional) 	: time differential of each step

		Returns:
			None
		'''
		assert num_steps > 0

		if not tau:
			tau 	= self.TAU

		log.note("Performing chemical dynamics (%d steps) ...", num_steps)

		# Perform n steps of the dynamics
		for n in range(num_steps):
			log.debugv("Chemical dynamics\t %d/%d", n+1, num_steps)

			# Loop over condensation reactions
			for cnd_reaction in self.condensations.values():
				cnd_reaction.perform_reaction(tau=tau, **self.RATES['cc'])

			# Loop over cleavage reactions
			for clv_reaction in self.cleavages.values():
				clv_reaction.perform_reaction(tau=tau, **self.RATES['cc'])

			# Adjust distribution between bound and unbound species, add food input (inside adjust_distributions) and apply the dynamics to the molecules
			for mol in self.all.values():
				mol.adjust_distributions(tau=tau,
				                         food_in=self.RATES['food_in'],
				                         **self.RATES['dd'])
				mol.apply_dynamics()

		log.debug("Performed chemical dynamics")

		return

	def update_candidates(self) -> bool:
		'''
		Check whether the concentration of a species in the inner circle is above threshold for the first time.

		Molecules are considered 'candidates', if they are in the inner circle but below the threshold. If their free concentration gets the first time above the threshold, they get moved to the outer circle, where it once again has the chance to form a new reaction.

		They are then no longer considered candidates.

		Returns True, if a molecule got above threshold. New reactions only need to be created, if new molecules got above threshold.
		'''

		T 	= self.C_THRS
		to_move = []

		for mol in self.inner.values():
			if not mol.is_cand:
				continue

			if mol.c_f > T:
				# set candidate status to False
				mol.set_cand(False)

				# queue to move to outer circle
				to_move.append(str(mol))

				log.debugv("Molecule  %s  over threshold", str(mol))

		# Now move the molecule (to not change the looping variable during the for loop above)
		for mol_struct in to_move:
			self.outer[mol_struct] 	= self.all[mol_struct]
			self.inner.pop(mol_struct)

		log.state("%d molecules got above threshold", len(to_move))

		return bool(len(to_move) > 0)

	def update_network(self, properties_only: bool=False) -> None:
		'''Builds the reaction and catalysis network by adding all reactions and the involved molecules. This way, molecules that do not take part in any reaction as product, educt, or catalyst will not be represented.

		Args:
			properties_only (bool) : only update the properties, do not update the network or detect cycles

		Returns:
			None
		'''

		if not self.network:
			log.debug("Skipping network update, because the network was disabled.")
			return

		if not properties_only:
			log.info("Updating network...")
			for rxn in self.condensations.values():
				self.network.add_reaction(rxn)

			for rxn in self.cleavages.values():
				self.network.add_reaction(rxn)

			log.debugv("Updated network.")

		self.network.update_properties()
		log.debugv("Updated network properties.")

		if not properties_only:
			self.network.run_analysis()
			log.debugv("Finished network analysis.")

		return

	# .........................................................................

	def log_state_info(self) -> None:
		'''Log some state variables'''

		log.state("# mol. species:     %d total,  %d inner,  %d outer",
		          len(self.all), len(self.inner), len(self.outer))

		cats 			= self.get_catalysts('all').values()
		num_cats		= len(cats)
		num_above_thrs	= sum([1 if c.c_f > self.C_THRS else 0 for c in cats])
		log.state("# cat. species:     %d total, %d above threshold",
		          					   num_cats, num_above_thrs)

		log.state("# reactions:        %d cond,  %d cleav",
		          len(self.condensations), len(self.cleavages))

		if cfg.cycle_det_enabled and self.network:
			log.state("# cycles:           " +
		    	      ",  ".join("{} {}".format(len(v), CYC_FMODES_MAP.get(k, k)) for k, v in self.network.cycles.items()))

		if self.network:
			log.state("Network size:       %d vertices,  %d edges",
			         self.network.num_vertices(),
			         self.network.num_edges())

		return

	# New reactions ...........................................................
	# ...also see private methods

	def new_condensation(self, e1: Molecule, e2: Molecule, cat: Molecule, target: str='temp', check_thrs: bool=True) -> Condensation:
		'''Create new condensation reaction from molecules e1 and e2 and catalyst cat and add the product to the temp circle.

		Runtime: O(1)

		Args:
			e1 (Molecule) : Educt 1
			e2 (Molecule) : Educt 2
			cat (Molecule) : Catalyst
			target (str) : where to place the product molecule
			check_thrs (bool) : whether all molecules need to be above threshold in order for the reaction to be created

		Return:
			bool : whether the reaction was created

		'''

		# The molecules need to be in this environment
		if DEBUG:
			if not (e1 in self.all.values()
			        and e2 in self.all.values()
			        and cat in self.all.values()):
				raise ValueError("Argument molecules e1, e2 and cat need to be part of the compartment.")

		# Randomly determine whether this catalyst catalyses this reaction
		if check_thrs:
			# All molecules taking part need to be above the threshold
			if not (e1.c_f > self.C_THRS and e2.c_f > self.C_THRS
			        and cat.c_f	> self.C_THRS):
				if DEBUG:
					log.debugv("Educt(s) or cat sub-threshold: %.5f, %.5f, %.5f", e1.c_f, e2.c_f, cat.c_f)
				return False

		# Create product structure and create molecule, if it does not exist
		p_struct= str(e1) + str(e2)

		if p_struct not in self.all:
			# no -> create and add to dict of all molecules to target region
			p 	= Molecule(p_struct, **self.MOLECULE_INIT)
			self.all[p_struct] 	= p
			getattr(self, target)[p_struct] = p
		else:
			# yep -> get from dict of all molecules
			p 	= self.all[p_struct]

		# Create new reaction, if it does not already exist
		r_str 	= R_STRF_FWD.format(str(e1), str(e2), p_struct)
		if r_str not in self.condensations:
			r_cond 	= Condensation(e1, e2, p, c_thrs=self.C_THRS)
			self.condensations[r_str] = r_cond
		else:
			r_cond 	= self.condensations[r_str]

		# Add catalyst to the reaction
		r_cond.add_catalyst(cat)

		if DEBUG:
			log.debugv("Initialised condensation: %s", r_cond)

		return r_cond

	def new_cleavage(self, e: Molecule, cat: Molecule, cut_idx: int, target: str='temp', check_thrs: bool=True) -> Cleavage:
		'''Creates a cleavage reaction from the educt molecule by cutting it at the given index

		# TODO doc
		'''

		# All molecules need to be in the compartment
		if DEBUG:
			if not (e in self.all.values() and cat in self.all.values()):
				raise ValueError("Argument molecules e and cat need to be part of the compartment.")

		if check_thrs:
			# All molecules taking part need to be above the threshold
			if not (e.c_f > self.C_THRS and cat.c_f > self.C_THRS):
				return False

		# Create product structure create the necessary molecules
		p1_struct 	= str(e)[:cut_idx]
		p2_struct 	= str(e)[cut_idx:]

		if p1_struct not in self.all:
			# no -> create and add to dict of all molecules and target region
			p1 	= Molecule(p1_struct, **self.MOLECULE_INIT)
			self.all[p1_struct] 	= p1
			getattr(self, target)[p1_struct] = p1
		else:
			# yep -> get from dict of all molecules
			p1 	= self.all[p1_struct]

		if p2_struct not in self.all:
			# no -> create and add to dict of all molecules and target region
			p2 	= Molecule(p2_struct, **self.MOLECULE_INIT)
			self.all[p2_struct] 	= p2
			getattr(self, target)[p2_struct] = p2
		else:
			# yep -> get from dict of all molecules
			p2 	= self.all[p2_struct]

		# Create new reaction, if it does not already exist
		r_str 	= R_STRF_REV.format(p1_struct, p2_struct, str(e))
		if r_str not in self.cleavages:
			r_clv 	= Cleavage(p1, p2, e, c_thrs=self.C_THRS)
			self.cleavages[r_str] = r_clv
		else:
			r_clv 	= self.cleavages[r_str]

		# Add catalyst to the reaction
		r_clv.add_catalyst(cat)

		return r_clv

	# Analysis ................................................................
	# TODO there is quite some optimisation potential here

	def is_static(self) -> bool:  # TODO implement other checks
		'''Checks whether the compartment is in a static state, i.e. there will be no future development'''

		if len(self.condensations) + len(self.cleavages) == 0:
			return True


		return False


	def is_exploding(self, mode: str, vals: dict, mean: int=1, condition: list=None): # TODO automate the checks / make them config-based?!
		''' Checks, whether this compartment is 'exploding', i.e. whether it is very quickly growing...

		Args:
			mode (str) 	: can be 'any', 'all', or 'condition'
			vals (dict)	: the values to check
			mean (int)  : over how many values to do the mean

		'''

		# Initialise stuff
		res 	= {} # for results (boolean values, where True means exploding)
		mean 	= min(mean, self.steps) # to not get issues in first steps
		mons 	= self.monitors # shortcut for self.monitors

		# Comparison methods, gets (float) value and (float) limit value
		_cmp_above 		= lambda val, lim: val > lim
		_cmp_below 		= lambda val, lim: val < lim
		_always_false	= lambda val, lim: False
		_always_true	= lambda val, lim: True

		# Check the values and build results dictionary
		for name, lim_val in vals.items():

			# Set defaults (so they don't have to be set in each case)
			vals = None 			# if not changed: not added to res dict
			_cmp = _always_false 	# as default: don't explode

			# For those variables working directly on a current monitor value
			if name == 'num_steps':
				vals = [self.steps]
				_cmp = _cmp_above

			elif name == 'num_mols':
				vals = mons['molecules']['all'][-mean:]
				_cmp = _cmp_above

			elif name == 'num_cats_at':
				vals = mons['molecules']['num_catalysts'][-mean:]
				_cmp = _cmp_above

			elif name == 'num_rxns':
				cond = mons['reactions']['condensations'][-mean:]
				clvs = mons['reactions']['cleavages'][-mean:]
				vals = np.array(cond) + np.array(clvs)
				_cmp = _cmp_above

			elif name == 'num_vertices':
				vals = mons['network']['num_vertices'][-mean:]
				_cmp = _cmp_above

			elif name == 'num_edges':
				vals = mons['network']['num_vertices'][-mean:]
				_cmp = _cmp_above

			elif name == 'num_cycles':
				vals = mons['cycles']['all'][-mean:]
				_cmp = _cmp_above

			# Those working on differentials ...
			# NOTE else-case not necessary, because defaults set above
			elif name == 'mols_ps':
				if self.steps > 1:
					vals = np.diff(mons['molecules']['all'][-(mean+1):])
					_cmp = _cmp_above

			elif name == 'rxns_ps':
				if self.steps > 1:
					cond = mons['reactions']['condensations'][-(mean+1):]
					clvs = mons['reactions']['cleavages'][-(mean+1):]
					vals = np.diff(np.array(cond) + np.array(clvs))
					_cmp = _cmp_above

			elif name == 'vertices_ps':
				if self.steps > 1:
					vals = np.diff(mons['network']['num_vertices'][-(mean+1):])
					_cmp = _cmp_above

			elif name == 'edges_ps':
				if self.steps > 1:
					vals = np.diff(mons['network']['num_edges'][-(mean+1):])
					_cmp = _cmp_above

			elif name == 'cycles_ps':
				if self.steps > 1:
					vals = np.diff(mons['cycles']['all'][-(mean+1):])
					_cmp = _cmp_above

			# Key did not match
			else:
				log.warning("Getting '%s' in explosion check is not implemented.", name)

			# Perform some checks
			if vals is None:
				# skip comparison (e.g. because the method was not implemented or because the step count is too small) and add False to result dict
				res[name] 	= False
				continue
			elif not len(vals):
				# still compare, but log
				log.warning("No values for '%s' in explosion checking. Comparison might fail...", name)

			# Pass values to comparison method and add result to dict
			res[name] 	= _cmp(np.mean(vals), lim_val)
			# End of loop iteration . . . . . . . . . . . . . . . . . . . . . .

		# Check the results according to the mode
		if mode == 'all':
			return all(res.values())

		elif mode == 'any':
			return any(res.values())

		elif mode == 'condition':
			# Evaluate the or-connected list by looping through the and-connected lists of conditions; as soon as one evaluates to True, the or-connected list also evaluates to True -> returns
			for and_conn in condition:
				if all([res[key] for key in and_conn]):
					print("true")
					return True
			return False

		else:
			raise ValueError("Invalid mode '{}', should be 'any', 'all', or 'condition'.".format(mode))


	def get_catalysts(self, src_pool: str) -> dict:
		'''Gets all catalyst molecules in the specified source pool.

		Runtime: O(n)
		'''

		pool 	= {}
		for mol in getattr(self, src_pool).values():
			if mol.is_cat:
				pool[str(mol)] = mol

		return pool

	def num_catalysts(self, above_thrs_only: bool=True) -> int:
		'''Returns the number of catalysts.'''
		cats 	= self.get_catalysts('all')

		if above_thrs_only:
			return sum([1 if c.c_f > self.C_THRS else 0 for c in cats.values()])
		else:
			return len(cats)


	def get_mass(self, only: str='all') -> tuple:
		'''Calculates the "mass" by summing up all concentrations

		Runtime: O(n)
		'''
		m_free 	= 0.
		m_bound = 0.

		for mol in self.all.values():
			if only == 'nonfood' and mol.is_food:
				continue
			elif only == 'food' and not mol.is_food:
				continue

			m_free 		+= mol.c_f * len(mol)
			m_bound 	+= mol.c_b * len(mol)

		return (m_free, m_bound)

	def free_mass(self, only: str='all') -> float:
		'''Calculates the free "mass" by summing up all free concentrations

		Runtime: O(n)
		'''
		return self.get_mass(only=only)[0]

	def bound_mass(self, only: str='all') -> float:
		'''Calculates the bound "mass" by summing up all bound concentrations

		Runtime: O(n)
		'''
		return self.get_mass(only=only)[1]

	def total_mass(self, only: str='all') -> float:
		'''Calculates the total "mass" by summing up all free and bound concentrations

		Runtime: O(n)
		'''
		masses 	= self.get_mass(only=only)
		return masses[0] + masses[1]


	def free_nonfood(self) -> float:
		''' '''
		return self.free_mass(only='nonfood')

	def bound_nonfood(self) -> float:
		''' '''
		return self.bound_mass(only='nonfood')

	def total_nonfood(self) -> float:
		''' '''
		return self.total_mass(only='nonfood')


	def free_food(self) -> float:
		''' '''
		return self.free_mass(only='food')

	def bound_food(self) -> float:
		''' '''
		return self.bound_mass(only='food')

	def total_food(self) -> float:
		''' '''
		return self.total_mass(only='food')

	# Analysis method .........................................................


	# Private methods .........................................................
	# TODO make public?
	def _create_condensations(self, e1_src: str, e2_src: str, cat_src: str, skip_length: int=0, target: str='temp', check_thrs: bool=True, check_individually: bool=False) -> None:
		'''Creates new condensation reactions from the source regions specified by the arguments.

		Runtime: O(n^2 * c) (n: #molecules, c: #catalysts)

		TODO Documentation
		'''

		e1_pool 	= list(getattr(self, e1_src).values())
		e2_pool 	= list(getattr(self, e2_src).values())
		cat_pool	= list(self.get_catalysts(cat_src).values())

		num_conds 	= len(self.condensations)

		if check_individually:
			# Go through all possible molecules and check for each combination, whether it will be catalysed
			for e1, e2 in itertools.product(e1_pool, e2_pool):
				# do not take into account condensations to a product below a certain length, e.g. to restrict condensations inside the food set
				if len(e1) + len(e2) <= skip_length:
					continue

				for cat in cat_pool:
					# Randomly determine whether this reaction is catalysed
					if np.random.random() < self.RXN_PROB:
						# yep -> add reaction
						self.new_condensation(e1, e2, cat,
					    	                  target=target,
					    	                  check_thrs=check_thrs)

		else:
			# Filter the pools to only include applicable molecules
			e1_pool  = filter_pool(e1_pool,  ['threshold'], c_thrs=self.C_THRS)
			e2_pool  = filter_pool(e2_pool,  ['threshold'], c_thrs=self.C_THRS)
			cat_pool = filter_pool(cat_pool, ['threshold'], c_thrs=self.C_THRS)

			# Calculate the number of reactions that will be created (given the probabilities), then choose randomly from the pools, which one catalyses which.
			aim_new_rxns	= len(e1_pool) * len(e2_pool) * len(cat_pool) * self.RXN_PROB
			new_rxns 		= []
			num_skips 		= 0


			while len(new_rxns) < aim_new_rxns:
				# take two random educts and a random catalyst to create the new reaction
				e1 	= np.random.choice(e1_pool)
				e2 	= np.random.choice(e2_pool)
				cat	= np.random.choice(cat_pool)

				# Save the combination of these three
				comb = (e1, e2, cat)
				if comb not in new_rxns:
					new_rxns.append(comb)
					self.new_condensation(e1, e2, cat,
				    	                  target=target, check_thrs=check_thrs)

				else:
					log.debugv("Combination (%s, %s, %s) already present among newly created reactions. Skipping.",
					           str(e1), str(e2), str(cat))
					num_skips += 1

					if num_skips >= self.MAX_PROB_SKIPS:
						log.warning("Was not able to create %d unique new reactions, could only create %d with %d skips.", aim_new_rxns, len(new_rxns), self.MAX_PROB_SKIPS)
						break

		if DEBUG:
			log.debugv("Created %5d new condensation reactions",
		    	       len(self.condensations) - num_conds)

		return

	def _create_cleavages(self, e_src: str, cat_src: str, target: str='temp', check_thrs: bool=True, check_individually: bool=False) -> None:
		'''Creates new cleaveage reactions from the source regions specified by the arguments and returns the number of created reactions.

		Runtime: O( TODO )
		'''
		e_pool 		= list(getattr(self, e_src).values())
		cat_pool	= list(self.get_catalysts(cat_src).values())

		num_clvs 	= len(self.cleavages)

		if check_individually:
			# Check if a cleavage reaction will be created individually for each possible combination in the pool
			for e in e_pool:
				e_len 	= len(e)

				# check for length-1 molecules
				if e_len <= 1:
					# can't be divided
					continue

				# loop over possible cut indices
				for cut_idx in range(1, e_len):
					# the cut is left of the index (python style)

					for cat in cat_pool:
						# Determine randomly if this reaction is catalysed
						if np.random.random() < self.RXN_PROB:
							self.new_cleavage(e, cat, cut_idx,
							                  target=target, check_thrs=check_thrs)

		else:
			# Filter the pools
			e_pool   = filter_pool(e_pool,   ['threshold', 'length'],
			                       c_thrs=self.C_THRS, 	   min_len=1)
			cat_pool = filter_pool(cat_pool, ['threshold'],
			                       c_thrs=self.C_THRS)

			# Calculate the number of reactions that will be created (given the probabilities), then choose randomly from the pools, which one catalyses which.
			aim_new_rxns	= len(e_pool) * len(cat_pool) * self.RXN_PROB
			new_rxns 		= []
			num_skips 		= 0

			while len(new_rxns) < aim_new_rxns:
				if num_skips >= self.MAX_PROB_SKIPS:
					log.warning("Was not able to create %d unique new reactions, could only create %d with %d skips.", aim_new_rxns, len(new_rxns), self.MAX_PROB_SKIPS)
					break

				# Choose components of the reaction randomly
				e 	= np.random.choice(e_pool)

				if len(e) <= 1:
					num_skips += 1
					continue

				cut_idx = np.random.randint(1,len(e))
				cat 	= np.random.choice(cat_pool)
				comb 	= (e, cat, cut_idx)

				if comb not in new_rxns:
					new_rxns.append(comb)
					self.new_cleavage(e, cat, cut_idx, target=target, check_thrs=check_thrs)
				else:
					num_skips += 1

		# Finished looping over all molecules in the educt pool
		if DEBUG:
			log.debugv("Created %5d new cleavage reactions",
		    	       len(self.cleavages) - num_clvs)

		return

	def _create_condensation_products(self, e1_src: str, e2_src: str, skip_length: int=0, target: str='temp') -> None:
		'''Creates all possible condensation products from the source regions and puts them into the target region. It does not require catalysts and does not create the corresponding reaction.

		Runtime: O(n^2) (n: #molecules)
		'''

		e1_pool 	= getattr(self, e1_src).values()
		e2_pool 	= getattr(self, e2_src).values()

		for e1, e2 in itertools.product(e1_pool, e2_pool):
			# do not take into account condensations to a product below a certain length, e.g. to restrict condensations inside the food set
			if len(e1) + len(e2) <= skip_length:
				continue

			# Create product structure
			p_struct = str(e1) + str(e2)

			# Check if the molecule already exists
			if p_struct not in self.all:
				# create and add to dict of all molecules and target region
				p 	= Molecule(p_struct, **self.MOLECULE_INIT)
				self.all[p_struct] 	= p
				getattr(self, target)[p_struct] = p
			else:
				# already exists, get from dict of all molecules
				p 	= self.all[p_struct]

		return

	def _set_concentration(self, m_src: str, concn: float, cats_only: bool=True) -> None:
		'''Sets the concentration of the molecules in the specified source region.'''

		m_pool 	= getattr(self, m_src).values()

		if cats_only:
			for m in m_pool:
				if m.is_cat:
					m.c_f 	= concn

		else:
			for m in m_pool:
				m.c_f 	= concn

		return


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Some helper methods

def filter_pool(pool:list, modes:list, c_thrs=None, min_len=None) -> list:
	''' Removes certain items from a molecule pool, depending on the selected mode.

	Available modes:
		'threshold' -- will remove those with free concentrations below kwargs['c_thrs']'''

	filtered 	= []

	for mol in pool:
		if 'threshold' in modes:
			if mol.c_f < c_thrs:
				continue

		if 'length' in modes:
			if len(mol) <= min_len:
				continue

		# did not see a 'continue' -> valid -> add to filtered list
		filtered.append(mol)

	return filtered

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' The ACN sub-package of the deeevolab package deals with everything necessary for the implementation of simulations of autocatalytic networks.'''


# Local Packages
from deeevolab.tools import get_config
from deeevolab.logging import setup_logger

# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# If using cython chemistry, setup pyximport
if cfg.use_cython_chemistry:
	import pyximport
	pyximport.install()

	from .cy_chemistry import Molecule, Condensation, Cleavage
else:
	from .chemistry import Molecule, Condensation, Cleavage

# Other imports
from .compartment import Compartment
from .molecule_monitor import MoleculeMonitor

# TODO make reasonable imports
# Important cannot import reaction network here, would break multiprocessing

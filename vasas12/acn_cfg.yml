# Configuration for all files in the deeevolab.acn package

# -----------------------------------------------------------------------------
# Package-level config entries ------------------------------------------------
# ...that can be linked below and are available in the __init__ file
# NOTE Careful. This can overwrite entries on the parent level!

use_cython_chemistry: &_cy_chem True

plot_every_n_steps: &_plot_every_n_steps False
save_every_n_steps: &_save_every_n_steps False

# To switch functionality on or off
enabled:
  network:    &_network_enabled     True
  cycle_det:  &_cycle_det_enabled   True

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
chemistry: &_chem

  tau: 0.0     # NOTE needs to be overwritten by project

  rxn:  &_rxn
    # Reaction string format
    strf: &_rxn_strf
      fwd: "{0:} + {1:} --> {2:}" # input: m1, m2, m3
      rev: "{2:} --> {0:} + {1:}" # input: m1, m2, m3

  # Regarding numerical calculation of the chemical dynamics: due to numerical uncertainties at higher tau, more molecules might be "reacting" then are present. The value below specifies the error tolerance of the concentration and should be selected at least two magnitudes smaller than the concentration threshold.
  # When the tolerance concentration is exceeded, a log.warning() is created.
  # Accuracy of calculation can be increased by reducing the time constant tau.
  c_warn_tol: 1.0e-7

# Link to cy_chemistry -> has the same config values.
cy_chemistry: *_chem

# .............................................................................
compartment:
  use_cython_chemistry: *_cy_chem

  rxn_strf: *_rxn_strf

  network_enabled:      *_network_enabled
  cycle_det_enabled:    *_cycle_det_enabled

  monitor_init: # Can add default monitors here
    reactions:
      enabled:            True
      name:               Reaction Number Monitor
      source:             self
      variables:
        - condensations
        - cleavages
      default_map_func:   len
      default_dtype:      uint32
      _needs_max_length:  True
      add_plot_kwargs:
        symlogy:    True
        linthreshy: 1
        ylabel:           Number of reactions
      plot_every_n_steps:    *_plot_every_n_steps

    molecules:
      enabled:            True
      name:               Molecule Species Monitor
      source:             self
      variables:
        - all
        - inner
        - outer
        - [num_catalysts, {'map_func': null}]
      default_map_func:   len
      default_dtype:      uint32
      _needs_max_length:  True
      add_plot_kwargs:
        symlogy:    True
        linthreshy: 10
        ylabel:           Number of species
      plot_every_n_steps:    *_plot_every_n_steps

    mass:
      enabled:            True
      name:               Mass Monitor
      source:             self
      variables:
        - total_mass
        # - free_mass
        - bound_mass
        - free_food
        - bound_food
        - total_nonfood
        - free_nonfood
        - bound_nonfood
      # default_map_func:
      default_dtype:      float64
      _needs_max_length:  True
      add_plot_kwargs:
        symlogy:    True
        linthreshy: 1.0e-5
        ylabel:           Mass
      plot_every_n_steps:    *_plot_every_n_steps

    network:
      enabled:            *_network_enabled
      name:               Network Monitor
      source:             self.network
      variables:
        - num_vertices
        - num_edges
      # default_map_func:
      default_dtype:      uint32
      _needs_max_length:  True
      add_plot_kwargs:
        symlogy:    True
        linthreshy: 50
        ylabel:           Count
      plot_every_n_steps:    *_plot_every_n_steps

# .............................................................................
molecule_monitor: {}

# .............................................................................
cycle_detection: &_cycle_det
  enabled: *_cycle_det_enabled
  method: self_built  # graph_tool or self_built
  method_kwargs:        # get passed to 'self_built' cycle detection method.
    max_stack_len:  14    # The max. stack length -> longer cycles not detected

  # The filter mode to use to create the vertex property for_cyc_det, which is then filtered for in the cycle detection -- if a different vertex property should be used for cycle detection, change the vtx_filter_vp to the desired property map or to null, if no filter should be applied
  vp_filter_mode: nonfood_above_thrs # nonfood, nonfood_above_thrs
  vp_filter:      for_cyc_det # the vertex property to use in cyc detection

  # The filters to be applied to the cycles found
  cyc_filter_modes:    # comment out those that should not be executed
    # - 'above_thrs'     # all reactions above threshold -- comment out if using vp_filter_mode nonfood_above_thrs (because 'above_thrs' has nothing to filter in that case)
    - catalytic      # at least one catalytic edge
    - autocatalytic  # every second edge is catalytic
    - viable_cores   # is a viable core (needs autocatalytic to be enabled)

  # The statistics to
  stat_kwargs:
    enabled: True
    modes: # will all be performed, comment out if not wanted
      - length
      - num_cat_edges

    mode_kwargs:
      _length: {}
      _num_cat_edges: {}

# .............................................................................
network:
  enabled: *_network_enabled

  cycle_det: *_cycle_det

  io:
    # default filename string format and extension
    # passed values: compartment & iteration & description string
    plot_file_strf: "c{:02d}_{:04d}{:}.pdf"

  # Network plotting defaults
  plt:
    enabled:      False  # whether to plot or not

    filter:
      enabled:    True
      # More specific
      leaves:     False
      above_thrs: True

    # layout
    layout:
      enabled:  &_layout True # if false, the one in graphviz_kwargs is used
      algo:     sfdp_layout
      args:     {}

    # default values for graph_tool.graphviz_draw() method
    graphviz_kwargs:
      layout:     neato
      pin:        *_layout
      overlap:    prism
      penwidth:   2
      sep:        0.2
      size:
        - 10
        - 16
      ratio: !expr 10/16
      # splines:    curved

    # default values for vertex and edge properties
    vprops:
      style:      filled

    eprops:
      len:        0.5
      dir:        forward
      arrowsize:  2.2

    # Plotting modes -- SELECT HERE!
    mode:
      nodes_by: concentration # nodetype, concentration
      edges_by: edgetype # edgetype

    show_label: True

    # parameters used for the different modes
    params:
      nodetype:  &_ntype
        shape:
          mol:  circle
          rxn:  square
          cond: square
          clv:  diamond
        size:
          mol:  0.7
          rxn:  0.35
        color:
          mol:  "#f1eef6"
          food: "#d7b5d8"
          cat:  "#df65b0"
          rxn:  black
        label_vprops:
          fontname:   Helvetica
          fontsize:   20
          fontcolor:  black
      v: *_ntype  # Shortcut

      edgetype: &_etype
        arrowhead:
          cat:  none
          rxn:  normal
        color:
          cat:  "#dd1c77"
          rxn:  "#777777"
      e: *_etype  # Shortcut

      concentration: &_vconc
        range:
          - 0.0
          - 2.0
        log:        True # linear (False) or log scale (True)
        cmap:       viridis

        rxn_color:  "#000000"  # the color for reaction nodes
        map_out_of_range: &_m_oor True # Whether to re-map out of range values
        sub_color:  "#000000"  # below range
        sup_color:  "#FFFFFF"  # above range

        scale:    3
        dpi:      72
        ax_pos:   [0.05, 0.07, 0.9, 0.02]
        extend:   *_m_oor
        cbar_label:   Concentration

        keep_tmp_cbar: True # Whether to keep the colorbar file once created or create a new one every time...

        nodelabel_vprops:
          fontname:   Helvetica
          fontsize:   20
          fontcolor:  "#F8F8F8"

      concn: *_vconc # shortcut

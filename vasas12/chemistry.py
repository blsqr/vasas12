#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''In this file, all chemistry-inspired classes and methods are defined, e.g. the Molecule and the Reaction classes.
Currently, the approach taken here is resembling an approach taken by Vasas et al., 2012'''


# Internal

# External
import numpy as np

# Local packages
from deeevolab.logging import setup_logger
from deeevolab.config import get_config

# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Some local constants
C_WARN_TOL 	= cfg.c_warn_tol
# -----------------------------------------------------------------------------

class Molecule:
	'''Class that represents a *type* of molecule, which can be described entirely by a bit pattern and its concentration. Note that the term 'species' is thus reduced to molecules which have exactly the same structure.
	'''

	def __init__(self, structure: str, c_f: float=None, cat_prob: float=None, is_food: bool=False, is_cand: bool=True) -> None:
		'''Initialise a Molecule object with a specific structure (which is fixed) and an initial concentration.

		Args:
			structure (str): The structure of the molecule as a bit pattern. The only allowed literals are 'a' and 'b' (to reduce confusion with binary strings and their leading zeros).

			c_f (float): The initial concentration of free molecules of this molecule species.

			cat_prob (float) : Probability to be a catalyst

			is_food (bool) : Whether the molecule is a food molecule or not

			is_cand (bool) : Whether the molecule is considered a candidate -- a molecule that has not been above threshold yet
		'''

		# Set attributes
		self.structure	= structure

		self.c_f 	= c_f
		self.c_b 	= 0.0

		# (temporary) variables for concentration change
		self.dc_f 	= 0.0
		self.dc_b 	= 0.0

		# Food or not?
		self.is_food= is_food

		# Candidate? A molecule is a candidate, if it was not above threshold yet. Newly created molecules (with 0 concentration) are always candidates
		self.is_cand= is_cand

		# Determine whether this is a catalyst, given by probability p' = p[0]
		self.is_cat = bool(np.random.random() < cat_prob)

		# The corresponding vertex object, if this molecule is inside the catalysis graph
		self.vtx 	= None

		return

	def __str__(self):
		return self.structure

	def __len__(self):
		return len(self.structure)

	def set_food(self, val: bool=True) -> None:
		'''Sets the molecule species to be a food species or not'''
		self.is_food 	= val
		return

	def set_cand(self, val: bool=True) -> None:
		'''Sets the candidate status'''
		self.is_cand 	= val

	def adjust_distributions(self, *args, tau: float, food_in: float, k_dis: float, k_dec: float) -> None:
		'''Adjusts the distributions between free and bound species of this molecule.'''

		# Adjust distributions
		self.dc_f 	+= tau * (k_dis  * self.c_b - k_dec * self.c_f)
		self.dc_b 	+= tau * (-k_dis * self.c_b - k_dec * self.c_b)

		# Add food input
		if self.is_food:
			self.dc_f 	+= tau * food_in

	def apply_dynamics(self) -> None:
		'''Applies the concentration changes calculated in the dynamics to the actual free and bound concentration of the molecules.

		Returns:
			None
		'''
		# Apply the values
		self.c_f 	+= self.dc_f
		self.c_b 	+= self.dc_b

		# Reset the temporary variables
		self.dc_f 	= 0.
		self.dc_b 	= 0.

		# Check for values being below zero and warn
		if self.c_f < 0.:
			# Two warning levels
			if C_WARN_TOL + self.c_f < 0.:
				log.warning("Numerical error tolerance 1e%.1f exceeded!\n\t c_f = -1e%.1f < 0 for molecule %s",
				            np.log(C_WARN_TOL)/np.log(10),
				            np.log(abs(self.c_f))/np.log(10),
				            self.structure)

			# Set to zero to avoid error propagation (switching signs)
			self.c_f 	= 0.

		if self.c_b < 0.:
			# Two warning levels
			if C_WARN_TOL + self.c_b < 0.:
				log.warning("Numerical error tolerance 1e%.1f exceeded!\n\t c_f = -1e^%.1f < 0 for molecule %s",
				            np.log(C_WARN_TOL)/np.log(10),
				            np.log(abs(self.c_b))/np.log(10),
				            self.structure)

			# set to zero to avoid error propagation (switching signs)
			self.c_b 	= 0.

		return


# -----------------------------------------------------------------------------

class Reaction:
	'''This class imlements an abstracted chemical reaction. It specialises on the case with only three participants.
	A reaction consists of three molecules and its direction; it can be simply defined by a string:

	r = 'aaba + ab 	--> aabaab'

	It always has the form
	    'm1   + m2 	--> m3' 		(forward, condensation)
	or 	'm3 		--> m1 + m2' 	(reverse, cleavage)

	Note that reactions are not commutative and 'ab + aaba -> abaaba' is a different reaction. Also, every reaction only takes place in one direction; the backreaction is a seperate reaction.'''

	def __init__(self, m1: Molecule, m2: Molecule, m3: Molecule, *args, c_thrs: float, direction: str='forward') -> None:
		'''Initialises a reaction object, which includes three molecule species and

		Args:
			m1 (Molecule) 	: molecule 1
			m2 (Molecule) 	: molecule 2
			m3 (Molecule) 	: molecule 3
			direction (str)	: 'forward' or 'reverse' reaction; this decides the shape of the reaction equation (1 + 2 -> 3 or 3 -> 1 + 2, respectively)
			c_thrs (float) 	: threshold concentration for the reaction to take place
		'''

		# Set attributes
		self.m1 		= m1
		self.m2 		= m2
		self.m3			= m3
		self.c_thrs 	= c_thrs  	# concentration threshold
		self.direction 	= direction

		self.mols 		= (self.m1, self.m2, self.m3) 	# for iterable access

		# Initialise list of catalysts
		self.cats 		= []

		# Save network vertex to reaction, for easier access
		self.vtx 		= None

		return

	def __str__(self):
		'''String formatting via reaction string format specified in config'''
		if self.is_fwd_reaction():
			return cfg.rxn.strf.fwd.format(self.m1, self.m2, self.m3)

		else:
			return cfg.rxn.strf.rev.format(self.m1, self.m2, self.m3)

	def add_catalyst(self, cat):
		'''Adds a molecule to the list of catalysts involved in this reaction, if it is not already present

		Args:
			cat (Molecule): The catalytic molecule to be added to this reaction
		'''
		if not isinstance(cat, Molecule):
			raise TypeError("Expected type Molecule, got {}".format(type(cat)))

		if cat not in self.cats:
			self.cats.append(cat)

	def above_thrs(self):
		'''Checks whether the free concentration of molecules is high enough for the reaction to take place.
		Important: Catalyst threshold needs to be checked seperately.

		Condition: ([m1]_f > T && [m2]_f > T) || [m3]_f > T

		Returns:
			bool : whether this reaction is above threshold'''

		return bool((self.m1.c_f > self.c_thrs and self.m2.c_f > self.c_thrs)
		            or self.m3.c_f > self.c_thrs)

	def perform_reaction(self):
		'''Dummy reaction'''
		raise NotImplementedError("Needs to be implemented in child class.")

	def is_fwd_reaction(self):
		return bool(self.direction == 'forward')


# More specific reactions - - - - - - - - - - - - - - - - - - - - - - - - - - -
class Condensation(Reaction):
	''' This child class of Reaction implements condensation reactions, i.e.:
	    e1 + e2 -> p1
	'''

	def __init__(self, e1, e2, p):
		'''Create a condensation reaction object, i.e. e1 + e2 -> p1

		Args:
			e1 (Molecule) : educt 1
			e2 (Molecule) : educt 2
			p  (Molecule) : product
		'''
		super().__init__(e1, e2, p, direction='forward')

		# Set names for internal use (pointers, no copies)
		self.e1 	= self.m1
		self.e2 	= self.m2
		self.p 		= self.m3

	def perform_reaction(self, *args, tau: float, cc_rates: dict) -> bool:
		'''Performs one timestep tau of the chemical dynamics of a catalysed condensation reaction.

		Note: The backwards reaction (cleavage, in this case) is not catalysed! # TODO check if this is correct

		Args:
			tau (float) : 	time differential to compute
			cc_rates (dict) : holds the condensation rate constant (k_cnd), cleavage rate consant (k_clf) and catalytic velocity (v_cat)

		Returns:
			bool : whether any reactions took place
		'''

		# Check if the molecule concentration is above threshold
		if not self.above_thrs():
			# Nope. Don't need to do anything else.
			return False

		# get values from arguments -- fails (delibaretly) when not present
		k_cnd 	= cc_rates['k_cnd']
		k_clv 	= cc_rates['k_clv']
		v_cat 	= cc_rates['v_cat']

		# Flag, whether any reactions took place
		_reacted 	= False

		# For shorter formulae (only referencing)
		e1, e2, p 	= self.e1, self.e2, self.p

		# Loop over all catalysts of this reaction, compute changes in concentrations, and save back to each molecule
		for cat in self.cats:
			if not cat.c_f > self.c_thrs:
				# skipping due to low catalyst concentration
				continue

			# Changes due to the forward reaction (condensation, catalysed)
			_v_fwd 	 = k_cnd * v_cat * (cat.c_f * e1.c_f * e2.c_f)
			e1.dc_f -= tau * _v_fwd
			e2.dc_f -= tau * _v_fwd
			p.dc_b	+= tau * _v_fwd

			# Changes due to the reverse reaction (cleavage, catalysed)
			_v_rev 	 = k_clv * v_cat * (cat.c_f * p.c_f)
			e1.dc_b += tau * _v_rev
			e2.dc_b += tau * _v_rev
			p.dc_f 	-= tau * _v_rev

			# Catalyst gets used up
			cat.dc_f+= - tau * (_v_fwd + _v_rev)
			cat.dc_b+= + tau * (_v_fwd + _v_rev)

			# This was a reaction --> set flag
			_reacted = True

		return _reacted


class Cleavage(Reaction):
	''' This child class of Reaction implements cleavage reactions, i.e.:
	    e1 -> p1 + p2.
	'''

	def __init__(self, p1, p2, e):
		'''Create a condensation reaction object, i.e. e -> p1 + p2
		Note the different order of the arguments, motivated by the 'reverse' keyword.

		Args:
			p1 (Molecule) : product 1
			p2 (Molecule) : product 2
			e  (Molecule) : educt
		'''
		super().__init__(p1, p2, e, direction='reverse')

		# Set names for internal use (pointers, no copies)
		self.p1 	= self.m1
		self.p2 	= self.m2
		self.e 		= self.m3

	def perform_reaction(self, *args, tau: float, cc_rates: dict):
		'''Performs one timestep tau of the chemical dynamics of a catalysed cleavage reaction.

		Note: The backwards reaction (condensation, in this case) is not catalysed! # TODO check if this is correct

		Args:
			tau (float) : 	time differential to compute
			cc_rates (dict) : holds the condensation rate constant (k_cnd), cleavage rate consant (k_clf) and catalytic velocity (v_cat)

		Returns:
			bool : whether any reactions took place
		'''

		# Check if the molecule concentration is above threshold
		if not self.above_thrs():
			# Nope. Don't need to do anything else.
			return False

		# get values from arguments -- fails (delibaretly) when not present
		k_cnd 	= cc_rates['k_cnd']
		k_clv 	= cc_rates['k_clv']
		v_cat 	= cc_rates['v_cat']

		# Flag, whether any reactions took place
		_reacted 	= False

		# For shorter formulae (only referencing)
		p1, p2, e 	= self.p1, self.p2, self.e

		# Loop over all catalysts of this reaction, compute changes in concentrations, and save back to each molecule
		for cat in self.cats:
			if not cat.c_f > self.c_thrs:
				# skipping due to low catalyst concentration
				continue

			# Changes due to the forward reaction (cleavage, catalysed)
			_v_fwd 	 = k_clv * v_cat * (cat.c_f * e.c_f)
			p1.dc_b += tau * _v_fwd
			p2.dc_b += tau * _v_fwd
			e.dc_f	-= tau * _v_fwd

			# Changes due to the reverse reaction (condensation, catalysed)
			_v_rev 	 = k_cnd * v_cat * (cat.c_f * p1.c_f * p2.c_f)
			p1.dc_f -= tau * _v_rev
			p2.dc_f -= tau * _v_rev
			e.dc_b 	+= tau * _v_rev

			# Catalyst gets used up
			cat.dc_f+= - tau * (_v_fwd + _v_rev)
			cat.dc_b+= + tau * (_v_fwd + _v_rev)

			# This was a reaction --> set flag
			_reacted = True

		return _reacted

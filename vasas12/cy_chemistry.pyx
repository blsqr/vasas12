#!/usr/bin/env python3
#cython: language_level=3, boundscheck=False, wraparound=False
# -*- coding: utf-8 -*-

'''In this file, all chemistry-inspired classes and methods are defined, e.g. the Molecule and the Reaction classes.
Currently, the approach taken here is resembling an approach taken by Vasas et al., 2012'''

# Internal

# External
import numpy as np

# Setup cython
cimport cython
from cpython cimport bool
# from cython.parallel import parallel, prange

# Import local packages
from deeevolab.logging import setup_logger
from deeevolab.config import get_config

# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Some local constants
C_WARN_TOL 	= cfg.c_warn_tol
# -----------------------------------------------------------------------------

cdef class Molecule:
	'''Class that represents a *type* of molecule, which can be described entirely by a bit pattern and its concentration. Note that the term 'species' is thus reduced to molecules which have exactly the same structure.
	'''

	cdef str structure
	cdef public bool is_food, is_cat, is_cand
	cdef public double c_f, c_b, dc_f, dc_b
	cdef public object vtx # TODO make pointer ??

	def __init__(self, str structure, double c_f=0.0, double cat_prob=0.0, bool is_food=False, bool is_cand=True):
		'''Initialise a Molecule object with a specific structure (which is fixed) and an initial concentration.

		Args:
			struct (str): The structure of the molecule as a bit pattern. The only allowed literals are 'a' and 'b' (to reduce confusion with binary strings and their leading zeros).

			c_f (float): The initial concentration of free molecules of this molecule species.

			cat_prob (float) : Probability to be a catalyst

			is_food (bool) : Whether the molecule is a food molecule or not

			is_cand (bool) : Whether the molecule is considered a candidate -- a molecule that has not been above threshold yet
		'''

		# Set attributes
		self.structure	= structure

		self.c_f 	= c_f
		self.c_b 	= 0.0

		# (temporary) variables for concentration change
		self.dc_f 	= 0.0
		self.dc_b 	= 0.0

		# Food or not?
		self.is_food= is_food

		# Candidate? A molecule is a candidate, if it was not above threshold yet. Newly created molecules (with 0 concentration) are always candidates
		self.is_cand= is_cand

		# Determine whether this is a catalyst, given by probability p' = p[0]
		self.is_cat = bool(np.random.random() < cat_prob)

		# The corresponding vertex object, if this molecule is inside the catalysis graph
		self.vtx 	= None

		return

	def __str__(self):
		return self.structure

	def __len__(self):
		return len(self.structure)

	cpdef set_food(self, bool val=True):
		'''Sets the molecule species to be a food species or not'''
		self.is_food 	= val

	cpdef set_cand(self, bool val=True):
		'''Sets the candidate status'''
		self.is_cand 	= val

	cpdef adjust_distributions(self, double tau=0.0, double food_in=0.0, double k_dis=0.0, double k_dec=0.0):
		'''Adjusts the distributions between free and bound species of this molecule and add the food input'''
		self.dc_f 	+= tau * (k_dis  * self.c_b - k_dec * self.c_f)
		self.dc_b 	+= tau * (-k_dis * self.c_b - k_dec * self.c_b)

		if self.is_food:
			self.dc_f 	+= tau * food_in

	cpdef apply_dynamics(self):
		'''Applies the concentration changes calculated in the dynamics to the actual free and bound concentration of the molecules.

		Returns:
			None
		'''
		# Apply the values
		self.c_f 	+= self.dc_f
		self.c_b 	+= self.dc_b

		# Reset the temporary variables
		self.dc_f 	= 0.
		self.dc_b 	= 0.

		# Check for values being below zero and warn
		if self.c_f < 0.:
			# Two warning levels
			if C_WARN_TOL + self.c_f < 0.:
				log.warning("Numerical error tolerance 1e%.1f exceeded!\n\t c_f = -1e%.1f < 0 for molecule %s",
				            np.log(C_WARN_TOL)/np.log(10),
				            np.log(abs(self.c_f))/np.log(10),
				            self.structure)

			# Set to zero to avoid error propagation (switching signs)
			self.c_f 	= 0.

		if self.c_b < 0.:
			# Two warning levels
			if C_WARN_TOL + self.c_b < 0.:
				log.warning("Numerical error tolerance 1e%.1f exceeded!\n\t c_f = -1e^%.1f < 0 for molecule %s",
				            np.log(C_WARN_TOL)/np.log(10),
				            np.log(abs(self.c_b))/np.log(10),
				            self.structure)

			# set to zero to avoid error propagation (switching signs)
			self.c_b 	= 0.

		return


# -----------------------------------------------------------------------------

cdef class Reaction:
	'''This class imlements an abstracted chemical reaction. It specialises on the case with only three participants.
	A reaction consists of three molecules and its direction; it can be simply defined by a string:

	r = 'aaba + ab 	--> aabaab'

	It always has the form
	    'm1   + m2 	--> m3' 		(forward, condensation)
	or 	'm3 		--> m1 + m2' 	(reverse, cleavage)

	Note that reactions are not commutative and 'ab + aaba -> abaaba' is a different reaction. Also, every reaction only takes place in one direction; the backreaction is a seperate reaction.'''

	cdef public Molecule m1, m2, m3
	cdef public float c_thrs
	cdef public str direction
	cdef public tuple mols
	cdef public object cats, vtx

	def __init__(self, m1, m2, m3, float c_thrs, str direction='forward'):
		'''Initialises a reaction object, which includes three molecule species and

		Args:
			m1 (Molecule) 	: molecule 1
			m2 (Molecule) 	: molecule 2
			m3 (Molecule) 	: molecule 3
			direction (str)	: 'forward' or 'reverse' reaction; this decides the shape of the reaction equation (1 + 2 -> 3 or 3 -> 1 + 2, respectively)
			c_thrs (float) 	: threshold concentration for the reaction to take place
		'''

		# Set attributes
		self.m1 		= m1
		self.m2 		= m2
		self.m3			= m3
		self.c_thrs 	= c_thrs
		self.direction 	= direction

		self.mols 		= (self.m1, self.m2, self.m3) 	# for iterable access

		# Initialise list of catalysts
		self.cats 		= []

		# Save network vertex to reaction, for easier access
		self.vtx 		= None

		return

	def __str__(self):
		'''String formatting via reaction string format specified in config'''
		if self.direction == 'forward':
			return cfg.rxn.strf.fwd.format(self.m1, self.m2, self.m3)

		else:
			return cfg.rxn.strf.rev.format(self.m1, self.m2, self.m3)

	def add_catalyst(self, cat):
		'''Adds a molecule to the list of catalysts involved in this reaction, if it is not already present

		Args:
			cat (Molecule): The catalytic molecule to be added to this reaction
		'''
		if not isinstance(cat, Molecule):
			raise TypeError("Expected type Molecule, got {}".format(type(cat)))

		if cat not in self.cats:
			self.cats.append(cat)

	cpdef bool above_thrs(self):
		'''Checks whether the free concentration of molecules is high enough for the reaction to take place. Note: Catalyst threshold needs to be checked seperately.
		(This method can be inherited.)

		Condition: ([m1]_f > T && [m2]_f > T) || [m3]_f > T
		Where [mN] is the sum of the bound and free concentrations

		Args:
			c_threshold (float) : threshold condensation

		Returns:
			bool : whether this reaction is above threshold'''

		return bool((self.m1.c_f > self.c_thrs and self.m2.c_f > self.c_thrs)
		            or self.m3.c_f > self.c_thrs)

	cpdef bool is_cond(self):
		# TODO ... make more abstract!
		return bool(self.direction == 'forward')


# More specific reactions - - - - - - - - - - - - - - - - - - - - - - - - - - -
cdef class Condensation(Reaction):
	''' This child class of Reaction implements condensation reactions, i.e.:
	    e1 + e2 -> p1
	'''

	cdef Molecule e1, e2, p

	def __init__(self, e1, e2, p, *args, c_thrs):
		'''Create a condensation reaction object, i.e. e1 + e2 -> p1

		Args:
			e1 (Molecule) : educt 1
			e2 (Molecule) : educt 2
			p  (Molecule) : product
		'''
		super().__init__(e1, e2, p, c_thrs=c_thrs, direction='forward')

		# Set names for internal use (pointers, no copies)
		self.e1 	= self.m1
		self.e2 	= self.m2
		self.p 		= self.m3

	cpdef bool perform_reaction(self, double tau=0.0, double k_cnd=0.0, double k_clv=0.0, double v_cat=0.0):
		'''Performs one timestep tau of the chemical dynamics of a catalysed condensation reaction.

		Note: The backwards reaction (cleavage, in this case) is not catalysed! # TODO check if this is correct

		Args:
			tau (float) : 	time differential to compute
			cc_rates (dict) : holds the condensation rate constant (k_cnd), cleavage rate consant (k_clf) and catalytic velocity (v_cat)

		Returns:
			bool : whether any reactions took place
		'''

		# Check if the molecule concentration is above threshold
		if not self.above_thrs():
			# Nope. Don't need to do anything else.
			return False

		# Flag, whether any reactions took place
		cdef bool _reacted 	= False

		# For shorter formulae (only referencing)
		e1, e2, p 	= self.e1, self.e2, self.p

		# Loop over all catalysts of this reaction, compute changes in concentrations, and save back to each molecule
		for cat in self.cats:
			if not cat.c_f > self.c_thrs:
				# skipping due to low catalyst concentration
				continue

			# Changes due to the forward reaction (condensation, catalysed)
			_v_fwd 	 = k_cnd * v_cat * (cat.c_f * e1.c_f * e2.c_f)
			e1.dc_f -= tau * _v_fwd
			e2.dc_f -= tau * _v_fwd
			p.dc_b	+= tau * _v_fwd

			# Changes due to the reverse reaction (cleavage, catalysed)
			_v_rev 	 = k_clv * v_cat * (cat.c_f * p.c_f)
			e1.dc_b += tau * _v_rev
			e2.dc_b += tau * _v_rev
			p.dc_f 	-= tau * _v_rev

			# Catalyst gets used up
			cat.dc_f+= - tau * (_v_fwd + _v_rev)
			cat.dc_b+= + tau * (_v_fwd + _v_rev)

			# This was a reaction --> set flag
			_reacted = True

		return _reacted


cdef class Cleavage(Reaction):
	''' This child class of Reaction implements cleavage reactions, i.e.:
	    e1 -> p1 + p2.
	'''
	cdef public Molecule p1, p2, e

	def __init__(self, p1, p2, e, *args, c_thrs):
		'''Create a condensation reaction object, i.e. e -> p1 + p2
		Note the different order of the arguments, motivated by the 'reverse' keyword.

		Args:
			p1 (Molecule) : product 1
			p2 (Molecule) : product 2
			e  (Molecule) : educt
		'''
		super().__init__(p1, p2, e, c_thrs=c_thrs, direction='reverse')

		# Set names for internal use (pointers, no copies)
		self.p1 	= self.m1
		self.p2 	= self.m2
		self.e 		= self.m3

	cpdef bool perform_reaction(self, double tau=0.0, double k_cnd=0.0, double k_clv=0.0, double v_cat=0.0):
		'''Performs one timestep tau of the chemical dynamics of a catalysed cleavage reaction.

		Note: The backwards reaction (condensation, in this case) is not catalysed! # TODO check if this is correct

		Args:
			tau (float) : 	time differential to compute
			cc_rates (dict) : holds the condensation rate constant (k_cnd), cleavage rate consant (k_clf) and catalytic velocity (v_cat)

		Returns:
			bool : whether any reactions took place
		'''

		# Check if the molecule concentration is above threshold
		if not self.above_thrs():
			# Nope. Don't need to do anything else.
			return False

		# Flag, whether any reactions took place
		cdef bool _reacted 	= False

		# For shorter formulae (only referencing)
		p1, p2, e 	= self.p1, self.p2, self.e

		# Loop over all catalysts of this reaction, compute changes in concentrations, and save back to each molecule
		for cat in self.cats:
			if not cat.c_f > self.c_thrs:
				# skipping due to low catalyst concentration
				continue

			# Changes due to the forward reaction (cleavage, catalysed)
			_v_fwd 	 = k_clv * v_cat * (cat.c_f * e.c_f)
			p1.dc_b += tau * _v_fwd
			p2.dc_b += tau * _v_fwd
			e.dc_f	-= tau * _v_fwd

			# Changes due to the reverse reaction (condensation, catalysed)
			_v_rev 	 = k_cnd * v_cat * (cat.c_f * p1.c_f * p2.c_f)
			p1.dc_f -= tau * _v_rev
			p2.dc_f -= tau * _v_rev
			e.dc_b 	+= tau * _v_rev

			# Catalyst gets used up
			cat.dc_f+= - tau * (_v_fwd + _v_rev)
			cat.dc_b+= + tau * (_v_fwd + _v_rev)

			# This was a reaction --> set flag
			_reacted = True

		return _reacted

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''In this file, everything network-related is defined.
Currently, the approach taken here is resembling an approach taken by Vasas et al., 2012.'''

# Internal
import os
import sys
import copy

# External
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from PyPDF2 import PdfFileWriter, PdfFileReader

# graph-tool
import graph_tool.all as gt

# Remove the gtk3+ error message from tty consoles # FIXME is ugly as hell!
if sys.stdout.isatty():
	try:
		_, TERM_COLS 	= os.popen('stty size', 'r').read().split()
		for _ in range(int(214/int(TERM_COLS)) + 1): # 214 char long err msg
			sys.stdout.write('\033[F'+'\x1b[2K\r') #go to previous line + clear
	except:
		pass
	sys.stdout.flush()

# Local Packages
from deeevolab.logging import setup_logger
from deeevolab.config import get_config
from deeevolab.tools import try_and_except
from .chemistry import Reaction, Molecule
from .cycle_detection import find_cycles, filter_cycles, cycle_stats

# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Some constants, that might be speed-critical
DEBUG 	= False 	# debug flag

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

class ReactionNetwork(gt.Graph):
	'''The ReactionNetwork consists of catalytic molecules (node type 1), reactions (node type 2) and the information which molecule catalyses which reaction (edges).

	It inherits from graph-tool's Graph class.

	The Reaction Network is a representation of the status of chemical reaction in each compartment; in the current implementation, the dynamics do not actually run on the graph, but it is merely an a posteriori representation of the state of the compartment.

	The network has two types of vertices (molecules and reactions) and two types of edges (connecting reactions with educts and products and connecting catalysts to reactions).

	All edges are directed. Reverse reactions have a separate reaction node and edge.
	'''

	def __init__(self, *args, comp, **kwargs):
		'''Initialises the CatalysisNetwork object, which is a Graph object with a few pre-initialised vertex and edge properties (see class docstring)'''
		super().__init__(*args, **kwargs)

		# The network knows in which compartment it is
		self.comp 	= comp

		# Vertex properties . . . . . . . . . . . . . . . . . . . . . . . . . .
		self.is_cat_vtx 	= self.new_vertex_property('bool')
		self.is_food_vtx	= self.new_vertex_property('bool')
		self.is_rxn_vtx		= self.new_vertex_property('bool')
		# if it is not a reaction, it is a molecule
		self.is_cond_vtx	= self.new_vertex_property('bool')
		# if it is not a condensation, it is a cleavage

		self.above_thrs 	= self.new_vertex_property('bool')
		# whether the reaction can take place -- if False, all molecules taking place in the reaction that are also below threshold are set to False

		self.is_leaf_vtx	= self.new_vertex_property('bool')
		# whether it is a leaf vertex

		self.for_cyc_det	= self.new_vertex_property('bool')
		# whether this vertex will be considered in the cycle detection

		# Some python objects
		self.reactions 		= self.new_vertex_property('object')
		self.molecules 		= self.new_vertex_property('object')

		# Relevant molecule data
		self.mol_struct 	= self.new_vertex_property('string')
		self.concn_free 	= self.new_vertex_property('float') # == py double
		self.concn_bound 	= self.new_vertex_property('float') # == py double


		# Edge properties . . . . . . . . . . . . . . . . . . . . . . . . . . .
		self.is_cat_edge	= self.new_edge_property('bool')
		# if it is not a catalysis edge, it's a reaction edge


		# Add some of these to the internal properties of the graph . . . . . .
		self.vertex_properties['is_cat_vtx'] 	= self.is_cat_vtx
		self.vertex_properties['is_food_vtx'] 	= self.is_food_vtx
		self.vertex_properties['is_rxn_vtx'] 	= self.is_rxn_vtx
		self.vertex_properties['is_cond_vtx'] 	= self.is_cond_vtx

		self.vertex_properties['above_thrs'] 	= self.above_thrs
		self.vertex_properties['is_leaf_vtx'] 	= self.is_leaf_vtx
		self.vertex_properties['for_cyc_det']	= self.for_cyc_det

		self.vertex_properties['mol_struct'] 	= self.mol_struct
		self.vertex_properties['concn_free']	= self.concn_free
		self.vertex_properties['concn_bound']	= self.concn_bound

		self.edge_properties['is_cat_edge'] 	= self.is_cat_edge

		# there is no point in saving the python objects, as they are not present after the graph is saved
		# . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
		# Set directed graph (easily changeable)
		self.set_directed(True)

		# Temporary position information attribute, used in plotting
		self.tmp_pos 	= None

		# For analysis results
		self.cycles 	= {'all': []} # dict for storing cycles
		self.cycle_stats= {'all': {}} # dict for storing cycle statistics
		for mode in cfg.cycle_det.cyc_filter_modes:
			self.cycles[mode] 		= []
			self.cycle_stats[mode]	= {}

		# Flag to check whether this graph was loaded and might thus not hold all data
		self.was_loaded = False

		return

	# TODO
	def __format__(self, spec: str):
		if spec == "":
			return ""
		else:
			return "NW_Format_NotImplemented"

	# Building the graph - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def add_reaction(self, rxn: Reaction, connect: bool=True, add_cats: bool=True, only_above_thrs: bool=True) -> bool:
		'''Adds a reaction vertex and the corresponding molecules to the catalysis graph.

		Args:
			rxn (Reaction) : the reaction vertex to add to the graph

			connect (bool) : Whether to connect the reaction to the graph and also add the missing products and/or educts to the graph

			add_cats (bool) : Whether to also add and connect the catalysts

			only_above_thrs (bool) : If True, only reactions which are considered chemically active are added to the graph.

		Returns:
			bool : whether the reaction was added (True) or was already present (False)

		'''

		if rxn.vtx is not None:
			if DEBUG:
				log.debugv("Reaction  %s  already present in network", rxn)
			return False

		if only_above_thrs:
			if not rxn.above_thrs():
				if DEBUG:
					log.debugv("Reaction  %s  is below threshold", rxn)
				return False

		# Add vertex and set property maps
		rxn_vtx 	= self.add_vertex()

		self.reactions[rxn_vtx] 	= rxn
		self.is_rxn_vtx[rxn_vtx] 	= True 		# is a reaction
		self.is_cat_vtx[rxn_vtx] 	= False 	# -> not a catalyst either
		self.is_food_vtx[rxn_vtx] 	= False 	# -> not food either
		self.is_leaf_vtx[rxn_vtx] 	= False 	# -> never a leaf
		self.is_cond_vtx[rxn_vtx]	= rxn.is_cond() # condensation?

		# Save to reaction object to avoid graph search
		rxn.vtx 	= rxn_vtx

		if DEBUG:
			log.debugv("Added reaction  %s  to network", rxn)

		if connect:
			# Add the products and educts to the graph (if not already present)
			for mol in rxn.mols:
				self.add_molecule(mol)

			# Add edges explicitly (do preserve directionality)
			if rxn.direction == "forward":
				# m1 + m2 --> m3
				self.add_reaction_edge(rxn.m1.vtx, 	rxn_vtx)
				self.add_reaction_edge(rxn.m2.vtx, 	rxn_vtx)
				self.add_reaction_edge(rxn_vtx,		rxn.m3.vtx)

			elif rxn.direction == "reverse":
				# m1 + m2 <-- m3
				self.add_reaction_edge(rxn.m3.vtx,	rxn_vtx)
				self.add_reaction_edge(rxn_vtx, 	rxn.m2.vtx)
				self.add_reaction_edge(rxn_vtx, 	rxn.m1.vtx)

			else:
				log.error("Reaction  %s  had illegal direction value!", rxn)

			log.debugv("Connected reaction to molecules")


		if add_cats:
			# Add the catalysts of the reaction to the graph
			for cat in rxn.cats:
				self.add_molecule(cat)

				# Add the edges, always pointing to the reaction vertex
				self.add_catalyst_edge(cat.vtx, rxn_vtx)

			log.debugv("Added %d catalyst edges to reaction", len(rxn.cats))

		return True

	def add_molecule(self, mol: Molecule) -> bool:
		'''Adds a molecule to the graph.

		Args:
			molecule (Molecule) : The molecule that is to be added to the graph.

		Returns:
			bool : whether the molecule was added (True) or was already present in the network (False)
		'''

		if mol.vtx is not None:
			if DEBUG:
				log.debugv("Molecule  %s  already present in network", mol)

			# Was called before -> not a leaf any more
			self.is_leaf_vtx[mol.vtx] 	= False
			# NOTE this is not the proper definition of a leaf

			return False

		# Add vertex and set properties
		mol_vtx 	= self.add_vertex()

		self.molecules[mol_vtx] 	= mol
		self.mol_struct[mol_vtx] 	= str(mol)
		self.is_rxn_vtx[mol_vtx] 	= False # is not reaction, thus a molecule
		self.is_cat_vtx[mol_vtx] 	= mol.is_cat
		self.is_food_vtx[mol_vtx] 	= mol.is_food

		self.is_leaf_vtx[mol_vtx] 	= True # first run -> is leaf

		# Save the vertex to the molecule to avoid graph searching
		mol.vtx 	= mol_vtx

		if DEBUG:
			log.debugv("Added molecule  %s  to the network", mol)
		return True

	def add_reaction_edge(self, source, target):
		'''Wrapper for the Graph.add_edge() method, which additionally adds the edge properties for a connection from a reaction to a product or educt.'''

		edge 	= self.add_edge(source, target, add_missing=False)

		self.is_cat_edge[edge] 	= False # ... because it is a reaction edge

		return edge

	def add_catalyst_edge(self, catalyst_vtx, reaction_vtx):
		'''Wrapper for the Graph.add_edge() method, which additionally adds the edge properties for a connection from a catalyst molecule vertex to a reaction vertex'''

		edge 	= self.add_edge(catalyst_vtx, reaction_vtx, add_missing=False)

		self.is_cat_edge[edge] 	= True # ... because it is a catalyst edge

		return edge

	def update_properties(self):
		'''Method that is called after every network update to update the values of property maps.'''
		log.debug("Updating network properties ...")
		self._update_vp_above_thrs()
		self._update_vp_concn()
		self._update_vp_cyc_det()

		return

	def run_analysis(self):
		''' Is called for the analysis step'''
		self.detect_cycles()

	# Analysing the graph - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def detect_cycles(self, vp_filter: str=cfg.cycle_det.vp_filter, method: str=cfg.cycle_det.method, method_kwargs: dict=cfg.cycle_det.method_kwargs, enabled: bool=cfg.cycle_det.enabled, filter_modes: list=cfg.cycle_det.cyc_filter_modes, stat_kwargs: dict=cfg.cycle_det.stat_kwargs):
		'''Uses the built-in topology.all_circuits method, based on the Hawick Enumerating Algorithm (2008), to get an iterator of all cycles in the graph, which is then counted.

		(Default values for optional arguments are set in the config file)

		Args:
			vtx_filter (str, optional) : name of the internal vertex property to filter the graph by prior to looking for cycles

			method (str, optional) : which implementation of the cycle detection algorithm to use -- options are 'graph_tool' for the internal algorithm, and 'self_built' for the self built one. Both are based on the Hawick James algorithm, but the latter one is considerably slower, while it is also more adaptable...

			method_kwargs (dict, optional) : is passed to the self-built cycle detection method

			enabled (bool, optional) : whether to run the cycle detection or skip it (if it takes too long)

			filter_modes (seq, optional) : names of the filters to apply on the list of cycles. Results are saved to the cycles attribute dict with the name of the filter mode as key.
		'''

		if not enabled:
			log.debug("Skipping cycle detection")
			return

		log.note("Detecting cycles ...")

		if vp_filter is None:
			gv 	= gt.GraphView(self, directed=True, skip_vfilt=True)

		elif isinstance(vp_filter, str):
			if vp_filter not in self.vertex_properties:
				raise ValueError("Vertex property '{}' is not present or not internal to the graph and can thus not be filtered for.\nAvailable internal vertex properties: {}".format(vp_filter, self.vertex_properties.keys()))

			log.debug("Vertex filter:  '%s'", vp_filter)

			gv 	= gt.GraphView(self, directed=True,
			                   vfilt=self.vertex_properties[vp_filter],
			                   skip_vfilt=True)

		# Find the cycles of the graph view using different methods
		if method == 'self_built':
			cycles 	= find_cycles(gv, **method_kwargs)
		elif method == 'graph_tool':
			cycles	= list(gt.all_circuits(gv))
		else:
			raise ValueError("Unknown cycle detection method '{}', choose from 'self_built', 'graph_tool'.".format(method))

		# Save
		self.cycles['all'] 	= cycles

		# Filter
		if isinstance(filter_modes, (list, tuple)):
			log.debug("Filtering %d cycles with %d different modes ...",
			          len(cycles), len(filter_modes))

			for mode in sorted(filter_modes):
				if mode == 'viable_cores' and 'autocatalytic' in self.cycles:
					# Autocatalytic cycles were already calculated -> use them as the basis for detection of viable cores
					# NOTE relies on alphabetical order of the keys
					self.cycles[mode]	= filter_cycles(gv, self.cycles['autocatalytic'], mode=mode)
				else:
					self.cycles[mode]	= filter_cycles(gv, cycles, mode=mode)

		# Calculate statistics
		log.debug("Calculating %d statistical measures on cycles ...",
		         len(stat_kwargs.get('modes', [])))
		for name, cyc in self.cycles.items():
			# ...overwrite previous results
			self.cycle_stats[name] = cycle_stats(gv, cyc, **stat_kwargs)

		return


	# In-/Output - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def load(self, file: str):
		''' Load a graph from a file.'''

		# Use the graph-tool method to load a Graph object
		g 	= gt.load_graph(file)

		# Carry over all attributes and link the internal ones
		self.__dict__.update(g.__dict__)
		self._link_properties()

		# Set flags and save original graph (to keep in scope, fails otherwise)
		self.was_loaded = True
		self._original_graph = g

		log.note("Loaded graph from  {}".format(file))

	def save(self, output: str, fmt: str='auto', new_vps: list=None):
		''' Saves the graph using the method inherited from gt.Graph, but also includes some vertex properties, which are not collected and updated before the graph is saved.
		'''

		if new_vps is None:
			new_vps 	= []

		if "concentration" in new_vps:
			# create property maps for concentrations and make internal
			c_f 	= self.new_vertex_property('float')
			c_b 	= self.new_vertex_property('float')

			self.vertex_properties['c_f'] 	= c_f
			self.vertex_properties['c_b'] 	= c_b

			# fill the values with the values from the molecules
			for vtx in self.vertices():
				if self.is_rxn_vtx[vtx]:
					continue

				mol 		= self.molecules[vtx]
				c_f[vtx] 	= mol.c_f
				c_b[vtx] 	= mol.c_b

		# Save
		super().save(output, fmt=fmt)

		# Delete the vertex properties again
		if "concentration" in new_vps:
			del(self.vertex_properties['c_f'])
			del(self.vertex_properties['c_b'])

	# TODO remove some config stuff?
	@try_and_except()
	def plot(self, output: str=None, output_strf: str=None, tmp_pos_mode=None, nodes_by: str=None, edges_by: str=None, subgraph: list=None, subgraph_kwargs:dict=None):
		'''Plots the network.

		Args:
			output (str) : the file to save it to

			pos (seq, optional) : the positions to use for the vertices

			subgraph (seq, optional) : a list of vertices to plot

			mode (str, optional) : which plotting mode to use (nodetype, concentration)
		'''

		if subgraph is None:
			log.note("Plotting ReactionNetwork ...")
		else:
			log.note("Plotting subgraph (length: %d) of ReactionNetwork ...", len(subgraph))

		# Work on graph view
		gv 	= gt.GraphView(self, directed=True)

		if isinstance(subgraph, (list, tuple, np.ndarray)):
			# Only plot a subgraph
			log.debug("Setting GraphView to subgraph with %d vertices.", len(subgraph))
			subgraph_vtx 	= gv.new_vertex_property('bool', val=False)

			for vtx in subgraph:
				subgraph_vtx[vtx] = True

			if subgraph_kwargs.get("add_neighbors", False):
				# Add the neighbor molecules to the reactions vertices
				log.debug("Adding neighbors to subgraph ...")
				for vtx in subgraph:
					if self.is_rxn_vtx[vtx]:
						for neighbor_vtx in self.vertex(vtx).all_neighbors():
							subgraph_vtx[neighbor_vtx] = True

			gv.set_vertex_filter(subgraph_vtx)

		elif cfg.plt.filter.enabled:
			# Further filtering... only possible when not on subgraph

			log.debug("Setting filtered GraphView ...")

			if cfg.plt.filter.leaves:
				gv.set_vertex_filter(self.is_leaf_vtx, inverted=True)
				log.debugv("Set leaf filter.")

			if cfg.plt.filter.above_thrs:
				gv.set_vertex_filter(self.above_thrs)
				log.debugv("Set above_thrs filter.")


		# Adjusting vertex and edge properties
		log.debugv("Adjusting vertex and edge properties...")

		# Get the default values
		vprops 	= copy.copy(cfg.plt.vprops)
		eprops 	= copy.copy(cfg.plt.eprops)

		# Distinguish some modes
		if nodes_by is None:
			nodes_by = cfg.plt.mode.nodes_by # default value

		if nodes_by == "nodetype":
			vprops, _vsize 	= _vp_by_nodetype(self, vprops)

		elif nodes_by == "concentration":
			vprops, _vsize 	= _vp_by_concentration(self, vprops)

		else:
			log.warning("No vertex mode %s implemented! Using defaults.",
			            nodes_by)

		if edges_by is None:
			edges_by = cfg.plt.mode.edges_by

		if edges_by == "edgetype":
			eprops 			= _ep_by_edgetype(self, eprops)

		else:
			log.warning("No edge mode %s implemented! Using defaults.",
			            cfg.plt.mode.edges_by)

		log.debugv("Adjusted vertex and edge properties")

		# Determine filename
		if output is None:
			# Try to generate a filename using the outdir and fname str formats
			if not isinstance(output_strf, str):
				raise ValueError("If argument output is not set, argument 'output_strf' needs to be passed as string.")
			output 	= output_strf.format(comp=self.comp, nw=self)

		# Check if output directory exists and create if not existant
		if not os.path.isdir(os.path.dirname(output)):
			try:
				os.mkdir(os.path.dirname(output))
			except FileExistsError:
				log.warning("The output directory of %s was already present.")

		# Determine layout, if it was not passed to the arguments
		if cfg.plt.layout.enabled and tmp_pos_mode is None:
			log.debug("Calculating optimal layout...")
			try:
				pos = getattr(gt, cfg.plt.layout.algo)(gv, **cfg.plt.layout.args)
			except AttributeError:
				log.warning("No layout algorithm with name %s found; layouting skipped", cfg.plt.layout.algo)
				pos = None

		elif tmp_pos_mode in ['read', 'both']:
			pos 	= self.tmp_pos
			log.debug("Vertex positions read from attribute.")

		else:
			pos 	= None

		# Plot using graphviz and save the positions, if needed to save
		pos 	= gt.graphviz_draw(gv,
		                 		   pos=pos,
		                 		   output=output,
		                 		   vprops=vprops,
		                 		   eprops=eprops,
		                 		   vsize=_vsize,
		                 		   **cfg.plt.graphviz_kwargs)

		if nodes_by == "concentration":
			_add_colorbar(output, **cfg.plt.params.concentration)

		if tmp_pos_mode in ['save', 'both']:
			self.tmp_pos 	= pos
			log.debug("Vertex positions saved to attribute.")

		log.debug("Saved plot to %s", output)

		# return the positions, so another plot can use the same ones
		return pos

	# Private methods  - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	def _link_properties(self):
		''' Links the internal properties of the graph to the attributes; this needs to be used after having loaded the graph.'''

		self.mol_struct = self.vp.mol_struct
		self.is_cat_vtx = self.vp.is_cat_vtx
		self.is_food_vtx= self.vp.is_food_vtx
		self.is_rxn_vtx	= self.vp.is_rxn_vtx
		self.is_cond_vtx= self.vp.is_cond_vtx

		self.is_cat_edge= self.ep.is_cat_edge

	# TODO speed up
	def _update_vp_above_thrs(self):
		''' Helper method to update the vertex property for molecules and reactions being above threshold.
		Reactions get True if their .above_thrs() attribute method returns True. If this returns False, Molecule vertices get set to False only when they themselves are below threshold.
		'''

		# Set all values to none, to be able to distinguish between visited and not visited vertices
		self.above_thrs 	= self.new_vertex_property('bool', val=None)
		self.vertex_properties['above_thrs'] = self.above_thrs # link to vp

		for vtx in self.vertices():
			if not self.is_rxn_vtx[vtx]:
				continue
			else:
				rxn_vtx = vtx
				rxn 	= self.reactions[rxn_vtx]

			if not rxn.above_thrs():
				# Molecules are sub-threshold -> set vp to False in reaction
				self.above_thrs[rxn_vtx] 	= False

				# ...and to False in the Molecules, if they themselves are sub-threshold, and have not been set to True by another reaction
				for mol_vtx in rxn_vtx.all_neighbors():
					mol 	= self.molecules[mol_vtx]
					if mol in rxn.mols and self.above_thrs[mol_vtx] is None:
						# TODO Check if this is a reasonable setting
						self.above_thrs[mol_vtx] = bool(mol.c_f + mol.c_b > self.comp.c_thrs)

			else:
				# Molecules are above the threshold -> check if catalysts are also
				cats_above_thrs = []
				for cat in rxn.cats:
					if cat.c_f > self.comp.C_THRS:
						cats_above_thrs.append(cat)

				if cats_above_thrs:
					# -> set the molecules and those catalysts to
					self.above_thrs[rxn_vtx] 	= True
					for mol_vtx in rxn_vtx.all_neighbors():
						mol 	= self.molecules[mol_vtx]
						if mol in rxn.mols or mol in cats_above_thrs:
							# set it to above threshold
							self.above_thrs[mol_vtx] = True

	def _update_vp_concn(self):
		''' Updates the concentration values of each molecule in the network.
		'''

		gv 	= gt.GraphView(self, skip_vfilt=True)
		gv.set_vertex_filter(gv.vp.is_rxn_vtx, inverted=True)

		for mol_vtx in gv.vertices():
			mol 	= self.molecules[mol_vtx] # to get the Molecule object
			self.concn_free[mol_vtx] 	= mol.c_f
			self.concn_bound[mol_vtx] 	= mol.c_b

		return

	def _update_vp_cyc_det(self, mode=cfg.cycle_det.vp_filter_mode):
		''' Update the vertex property that is used for the cycle detection graph view.
		'''
		if mode == 'nonfood':
			for vtx in self.vertices():
				self.vp.for_cyc_det[vtx] = not self.vp.is_food_vtx[vtx]

		elif mode == 'nonfood_above_thrs':
			for vtx in self.vertices():
				self.vp.for_cyc_det[vtx] = (not self.vp.is_food_vtx[vtx]
				                            and self.vp.above_thrs[vtx])
		else:
			raise ValueError("Invalid mode argument for vertex property update for cycle detection. Needs to be 'nonfood' or 'nonfood_above_thrs', was '{}'.".format(mode))

		return

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Some methods to relate vertex and edge properties to, e.g., vertex size ... for nicer plotting.

def _vp_by_nodetype(g, vprops):
	''' '''

	_shape 		= g.new_vertex_property("string")
	_vsize 		= g.new_vertex_property("float")
	_vcolor 	= g.new_vertex_property("string")

	for vtx in g.vertices():
		_vsize[vtx] 	= _nodetype_to_size( g.is_rxn_vtx[vtx])
		_shape[vtx] 	= _nodetype_to_shape(g.is_rxn_vtx[vtx],
		                               		 g.is_cat_vtx[vtx],
		                               		 g.is_food_vtx[vtx],
		                               		 is_cond=g.is_cond_vtx[vtx])
		_vcolor[vtx] 	= _nodetype_to_color(g.is_rxn_vtx[vtx],
		                               		 g.is_cat_vtx[vtx],
		                               		 g.is_food_vtx[vtx])

	# Set style properties of vertices
	vprops.update({
		'shape': 		_shape,
		'fillcolor': 	_vcolor
	})

	# Add label to the node
	if cfg.plt.show_label:
		vprops['label'] = g.vp.mol_struct
		vprops.update(cfg.plt.params.nodetype.label_vprops)

	return vprops, _vsize

def _vp_by_concentration(g, vprops):
	''' '''

	_shape 		= g.new_vertex_property("string")
	_vsize 		= g.new_vertex_property("float")
	_vcolor 	= g.new_vertex_property("string")

	for vtx in g.vertices():
		_vsize[vtx] 	= _nodetype_to_size( g.is_rxn_vtx[vtx])
		_shape[vtx] 	= _nodetype_to_shape(g.is_rxn_vtx[vtx],
		                               		 g.is_cat_vtx[vtx],
		                               		 g.is_food_vtx[vtx],
		                               		 is_cond=g.is_cond_vtx[vtx])
		_vcolor[vtx] 	= _concentration_to_color(g, vtx)

	# Set style properties of vertices
	vprops.update({
		# 'label':		g.vp.mol_struct, # no label
		'shape': 		_shape,
		'fillcolor': 	_vcolor
	})

	# Add label to the node
	if cfg.plt.show_label:
		vprops['label'] = g.vp.mol_struct
		vprops.update(cfg.plt.params.concn.nodelabel_vprops)

	return vprops, _vsize

def _ep_by_edgetype(g, eprops):
	''' '''
	_ecolor 	= g.new_edge_property("string")
	_arrhead 	= g.new_edge_property("string")

	for edge in g.edges():
		_arrhead[edge] = _edgetype_to_arrhead(g.is_cat_edge[edge])
		_ecolor[edge]  = _edgetype_to_color(g.is_cat_edge[edge])


	eprops.update({
		'arrowhead': 	_arrhead,
		'color': 		_ecolor
	})

	return eprops

# -----------------------------------------------------------------------------
# mapping functions -----------------------------------------------------------
# nodetpye . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
NODETYPE_PARAMS 	= cfg.plt.params.nodetype

def _nodetype_to_color(is_rxn, is_cat, is_food):
	# TODO adjust this by number of connections and with a colormap
	if not is_rxn:
		if is_cat and not is_food:
			# non-food catalysts get a special color
			return NODETYPE_PARAMS['color']['cat']
		else:
			if is_food:
				return NODETYPE_PARAMS['color']['food']
			else:
				return NODETYPE_PARAMS['color']['mol']

	else:
		return NODETYPE_PARAMS['color']['rxn']

def _nodetype_to_size(is_rxn):
	if not is_rxn:
		return NODETYPE_PARAMS['size']['mol']
	else:
		return NODETYPE_PARAMS['size']['rxn']

def _nodetype_to_shape(is_rxn, is_cat, is_food, is_cond=None):
	# TODO add food here?
	if not is_rxn:
		# is molecule
		return NODETYPE_PARAMS['shape']['mol']
	else:
		# is reaction
		if is_cond is None:
			return NODETYPE_PARAMS['shape']['rxn']
		elif is_cond:
			return NODETYPE_PARAMS['shape']['cond']
		else:
			return NODETYPE_PARAMS['shape']['clv']

# concentration. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
C2COLOR_PARAMS 	= cfg.plt.params.concn

def _concentration_to_color(g, vtx, mode: str='free'):
	'''Gets passed a vertex and returns a color according to the concentration of the molecule or a different color, if it is a reaction vertex.'''

	# is the node even a molecule?
	if g.is_rxn_vtx[vtx]:
		# it's a reaction vertex
		return C2COLOR_PARAMS['rxn_color']

	# Read concentration from vertex property
	if mode == 'free':
		c 		= g.vp.concn_free[vtx]
	elif mode == 'bound':
		c 		= g.vp.concn_bound[vtx]
	elif mode == 'total':
		c 		= g.vp.concn_free[vtx] + g.vp.concn_bound[vtx]
	else:
		raise ValueError("Mode argument selecting concentration to calculate color from needs to be 'free', 'bound' or 'total'.")

	# Get values
	rg 		= C2COLOR_PARAMS["range"]	# (lower bound, upper bound)
	cmap 	= mpl.cm.get_cmap(C2COLOR_PARAMS["cmap"])

	# Map values to colormap value in range [0,1]
	if not C2COLOR_PARAMS['log']:
		# linear mapping to color map value
		cval 	= (c - rg[0])/(rg[1] - rg[0])
	else:
		# log mapping
		if c > 0.:
			log_rg 	= (np.log(rg[0]), np.log(rg[1]))
			cval 	= (np.log(c) - log_rg[0]) / (log_rg[1] - log_rg[0])
		else:
			cval 	= 0.


	if not C2COLOR_PARAMS["map_out_of_range"]:
		cval 	= min(1, max(0, cval))		# restricting to [0,1]
	else:
		if cval < rg[0]:
			return C2COLOR_PARAMS["sub_color"]
		elif cval > rg[1]:
			return C2COLOR_PARAMS["sup_color"]

	# Return as hex value
	return mpl.colors.rgb2hex(cmap(cval))

# edgetype . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
EDGETYPE_PARAMS 	= cfg.plt.params.edgetype

def _edgetype_to_color(is_cat):
	if is_cat:
		# is a catalysis edge
		return EDGETYPE_PARAMS['color']['cat']
	else:
		# is a reaction edge
		return EDGETYPE_PARAMS['color']['rxn']

def _edgetype_to_arrhead(is_cat):
	if is_cat:
		# is a catalysis edge
		return EDGETYPE_PARAMS['arrowhead']['cat']
	else:
		# is a reaction edge
		return EDGETYPE_PARAMS['arrowhead']['rxn']

# hacks . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

def _add_colorbar(target_pdf, keep_tmp_cbar: bool=False, **params): # TODO clean up
	''' Hack your way to a pdf with a colorbar'''

	log.debug("Adding colorbar to %s ... (keep_tmp_cbar: %s)",
	          target_pdf, str(keep_tmp_cbar))

	# The target pdf to add the colorbar to
	tpdf 	= PdfFileReader(open(target_pdf, 'rb'))
	graph 	= tpdf.getPage(0)

	# Parameters for the colorbar pdf
	scale	= params.get('scale', 	3)
	dpi 	= params.get('dpi', 	72)
	axpos 	= params.get('ax_pos', 	[0.05, 0.05, 0.9, 0.02])

	figsize	= (graph.mediaBox[2]/dpi*scale, graph.mediaBox[3]/dpi*scale)

	# Where to save/read the colorbar from
	cbar_pdf 	= os.path.dirname(target_pdf) + "/cbar.tmp.pdf"

	# The temporary colorbar pdf only needs to be created, if it does not already exist
	if not (keep_tmp_cbar and os.path.isfile(cbar_pdf)):
		# The file is not there yet or  it is supposed to be created again

		# Make a figure and axes with dimensions as desired
		fig 	= plt.figure(figsize=figsize)
		ax 		= fig.add_axes(axpos)

		# Create the colormap
		cmap 	= mpl.cm.get_cmap(params['cmap'])
		if params.get('extend'):
			if params.get('sup_color') is not None:
				cmap.set_over(mpl.colors.hex2color(params.get('sup_color')))

			if params.get('sub_color') is not None:
				# FIXME set_under is somehow not applied
				cmap.set_under(mpl.colors.hex2color(params.get('sub_color')))

		if not params.get('log'):
			norm 	= mpl.colors.Normalize(vmin=params['range'][0],
			                               vmax=params['range'][1])
		else:
			norm 	= mpl.colors.LogNorm(vmin=params['range'][0],
			                             vmax=params['range'][1])

		extend	= "both" if params.get("extend") else "neither"
		cbar 	= mpl.colorbar.ColorbarBase(ax, cmap=cmap, norm=norm,
		                                 	orientation='horizontal',
		                     			 	extend=extend)
		cbar.set_label(params.get('label', ''))

		# save the colorbar as pdf
		fig.savefig(cbar_pdf)
		plt.close('all')

	# Read colorbar pdf
	cpdf = PdfFileReader(open(cbar_pdf, 'rb'))
	cbar = cpdf.getPage(0)

	# Merge them together
	cbar.mergeScaledPage(graph, scale)
	output 	= PdfFileWriter()
	output.addPage(cbar)

	os.remove(target_pdf) 	# overwriting causes bugs, delete first

	with open(target_pdf, 'wb') as f:
		output.write(f)

	if not keep_tmp_cbar:
		os.remove(cbar_pdf)

	return


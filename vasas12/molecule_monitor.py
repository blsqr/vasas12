#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' A monitor to monitor the properties of Molecule objects.'''

# Internal

# External

# Local Packages
from deeevolab.logging import setup_logger
from deeevolab.config import get_config
from deeevolab.monitor import BinaryMonitor

# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# -----------------------------------------------------------------------------
class MoleculeMonitor(BinaryMonitor):
	''' This class is a child of the StateMonitor class and specialises on recording several different molecules of a certain type. It does so by overwriting the recording methods.

	source: 	this is the **dict-like** object to check for the molecules

	variables:	the molecule names

	select_attr:	the attribute of the molecules that (if true) select them for being tracked by this monitor

	rec_attr: 	which attribute to record
	'''
	def __init__(self, *args, select_attr=None, rec_attr=None, **kwargs):

		super().__init__(*args, **kwargs)

		if select_attr is None or rec_attr is None:
			raise ValueError("Arguments select_attr and rec_attr need to be given, were '%s' and '%s'.", select_attr, rec_attr)

		self.select_attr	= select_attr
		self.rec_attr 		= rec_attr

		log.debug("MoleculeMonitor initialised.")

	def add_variable(self, var_name: str, **kwargs):
		''' Add a molecule to the monitor'''
		super().add_variable(var_name, **kwargs)

		# Overwrite value getter
		self.variables[var_name]['get_val'] 	= self._get_val_by_rec_attr

	def record_single_step(self):
		''' Updates the molecules and then calls the parent monitor to record a step.'''
		if self.enabled and self.record_mode == 'get':
			self.update_molecules()

		super().record_single_step()

	# TODO improve / speed up select_attr checks
	def update_molecules(self):
		''' Checks which molecules need to be added to the variables'''
		log.debug("Updating variables for MoleculeMonitor ...")

		num_mols 	= len(self.variables)

		log.state("tracked molecules:  %d\tpossible:  %d", num_mols, len(self.source))

		# Go over all molecules in the source object
		for mol_name in self.source.keys():
			if mol_name not in self.variables.keys():
				# Get the attribute that will be used for selection
				select_attr = getattr(self.source[mol_name], self.select_attr)

				if isinstance(select_attr, bool):
					if select_attr:
						self.add_variable(mol_name)

				elif callable(select_attr):
					if select_attr.__call__():
						self.add_variable(mol_name)

		log.state("Added %d new molecules to the monitor.", len(self.variables) - num_mols)

	def manual_record_step(self, data):
		''' Manual recording is not possible for this monitor ...'''
		raise NotImplementedError("Cannot manually record Molecule Monitor.")

	def _get_val_by_rec_attr(self, var_name):
		''' Helper function to get a value by the record attribute 'rec_attr' of the MoleculeMonitor...
		'''
		mol 	= self.source[var_name]

		if callable(getattr(mol, self.rec_attr)):
			val 	= getattr(mol, self.rec_attr).__call__()

		else:
			val 	= getattr(mol, self.rec_attr)

		map_func 	= self.variables[var_name]['map_func']
		if map_func is not None:
			val 	= map_func(val)

		return val

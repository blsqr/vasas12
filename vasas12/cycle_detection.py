#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Internal
import copy
from collections import defaultdict

# Setup cython
# cimport cython
# from cython.parallel import parallel, prange

# Local Packages
from deeevolab.logging import setup_logger
from deeevolab.config import get_config

# Initialise loggers and config file
log 	= setup_logger(__name__)
cfg 	= get_config(__name__)
log.debug("Loaded module, logger, and config.")

# Constants
DEBUG 		= False 	# debug flag
STAT_MODES 	= ['length', 'num_cat_edges']

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------

def find_cycles(graph, convert_back: bool=False, max_stack_len: int=None):
	''' Returns a list of cycles in the graph.

	Args:
		graph (Graph) : graph_tool Graph object

		convert_back (bool, optional) : whether to convert the vertex indices back to vertex objects # TODO Implement

		max_stack_len (int, optional) : whether to limit the stack length (limiting detectable cycles to those up to this length)
	'''

	def unblock(vtx):
		''' Unblocks the vertex with the passed id. Additionally, it goes through B and unblocks all those in the chain ...'''
		blocked[vtx] = False

		while len(B[vtx]) > 0:
			w = B[vtx][0] # start with first val ... (does not matter too much)
			B[vtx] = [v for v in B[vtx] if v != w]
			if blocked[w]:
				unblock(w)

	def find_cycle(graph, vtx):
		''' The recursively called method to find circuits in the graph, starting from vtx.

		Shared variables: graph, B, accs, start_vtx, stack
		'''

		# Set flag to show whether a circuit was found in this recursion
		circuit_found 	= False

		# Put the current vertex onto the stack and set it blocked
		stack.append(vtx)
		blocked[vtx] 	= True

		# Compile list of target nodes (for speedup, because needed twice)
		out_neighbors = [graph.vertex_index[i] for i in graph.vertex(vtx).out_neighbors()]

		# Iterate over target nodes, which are the outgoing edges of this vertex
		for target_vtx in out_neighbors:
			if max_stack_len and len(stack) > max_stack_len:
				# do not continue with the recursion
				break # -> find circuit will not be called again

			if target_vtx < start_vtx:
				# The circuits that are not looked at here will be found from a different start node. Not having this leads to shifted duplicates of the same circuits
				continue

			if target_vtx == start_vtx:
				# We have a circuit!
				circuit_found = True
				accs.append(copy.copy(stack))

			elif not blocked[target_vtx]: # --> target > start == True
				# This node is not blocked -> recursively continue the search
				if find_cycle(graph, target_vtx):
					circuit_found = True

		# Distinguish into the cases where circuit was found and not
		if circuit_found:
			unblock(vtx)
			# ...such that it can be included in other circuits

		else:
			# iterate over all target nodes
			for target_vtx in out_neighbors:
				if target_vtx < start_vtx:
					# don't look at that part of the graph (same as above)
					continue

				if vtx not in B[target_vtx]:
					B[target_vtx].append(vtx)
					# NOTE B is defaultdict -> this is the time-consuming step

		stack.pop() # important, because the recursion works on this stack

		return circuit_found

	# -------------------------------------------------------------------------
	# Initialise variables
	num_vtcs 	= graph.num_vertices()
	accs 		= [] # list of the found autocatalytic cycles

	_blocked 	= {graph.vertex_index[vtx]:False for vtx in graph.vertices()}
	# ...to use for reintialisation of block list

	log.debug("Looking for autocatalytic cycles in graph with %d vertices", num_vtcs)

	for start_vtx in graph.vertices():
		# (Re-)initialise variables
		B = defaultdict(list) # this step is fast
		blocked = copy.copy(_blocked) # faster then re-creating
		stack 	= []

		# Search for circuits starting at this vertex (by index, not object)
		find_cycle(graph, graph.vertex_index[start_vtx])

	if convert_back:
		# Convert the indices back to Vertex objects of the argument graph
		raise NotImplementedError()

	return accs


def filter_cycles(graph, cycles: list, *_, mode: str):
	''' Filters the list of cycles in the graph depending on different modes and returns the filtered list of cycles.

	Args:
		graph (ReactionNetwork) : The graph it all takes place on
		cycles (seq) : The sequence of cycles found in the graph
		mode (str) : The mode to use for filtering.
			Available modes: 'above_thrs', 'catalytic', 'viable_cores'

	Return:
		list of cycles that fit the filtering mode.
	'''
	MODES = ['above_thrs', 'catalytic', 'autocatalytic', 'viable_cores']

	if mode not in MODES:
		raise ValueError("Mode '{}' invalid, choose from {}.".format(mode, MODES))

	log.debug("Filtering %d cycles using mode '%s' ...", len(cycles), mode)

	f_cycs = [] # the list that is to be populated

	if mode == 'above_thrs':
		# Loop over cycles and check if all reactions are above threshold
		for cyc in cycles:
			for vtx in cyc:
				if graph.vp.is_rxn_vtx[vtx] and not graph.vp.above_thrs[vtx]:
					# is a reaction, but not above threshold -> break
					break
			else:
				# Finished -> is a cycle with all reactions above threshold
				f_cycs.append(cyc)

	elif mode == 'catalytic':
		# Loop over cycles and check if they have a catalytic edge
		for cyc in cycles:
			if num_catalytic_edges(graph, cyc) > 0:
				f_cycs.append(cyc)

	elif mode == 'autocatalytic':
		# Loop over cycles and check if half the edges of the cycle are catalytic, i.e. if the cycle is autocatalytic
		for cyc in cycles:
			if num_catalytic_edges(graph, cyc) == len(cyc)//2:
				# NOTE the cycle length is always an even number, allowing to use integer division
				f_cycs.append(cyc)

	elif mode == 'viable_cores':
		# Loop over cycles and determine whether they are produced directly or indireactly (but through catalysed reactions) by food molecules, thus making them viable cores
		for cyc in cycles:
			if is_viable(graph, cyc):
				f_cycs.append(cyc)

	log.debug("Found %d cycles in mode '%s'.", len(f_cycs), mode)

	return f_cycs


def num_catalytic_edges(graph, cycle):
	''' Returns the number of catalytic edges of the cycle in the given graph.

	Args:
		graph (ReactionNetwork) : the graph to work on
		cycle (seq of Vertex objects or indices) : the cycle to check

	Return:
		int : number of catalytic edges in the cycle
	'''

	num_cat_edges 	= 0
	shifted_cycle	= list(cycle[1:]) + list(cycle[:1])

	# Iterate over pairs of vertex (start) and the next vertex (finish)
	for vtx, nxt_vtx in zip(cycle, shifted_cycle):
		# Make sure these are vertices
		vtx 	= graph.vertex(vtx)
		nxt_vtx = graph.vertex(nxt_vtx)

		# Check edges
		for edge in vtx.all_edges():
			if not (edge.source() == vtx and edge.target() == nxt_vtx):
				# not a relevant edge
				continue

			else:
				if graph.ep.is_cat_edge[edge]:
					# is a catalytic edge
					num_cat_edges += 1

	return num_cat_edges

def is_viable(graph, cycle): # TODO consider "extended" food set?
	''' Looks at the nodes in the cycle and for every reaction determines if they are food-catalysed or food-sourced'''

	# Iterate over all edges
	for vtx in cycle:
		vtx 	= graph.vertex(vtx)

		# Do not look at molecule vertices
		if not graph.vp.is_rxn_vtx:
			continue

		# Get the input molecules to this reaction by looking at incoming edges
		for edge in vtx.all_edges():
			if (not edge.target() == vtx) or graph.ep.is_cat_edge[edge]:
				# Skip outgoing and catalytic edges
				continue

			# Is an input molecule -> Check if it is food
			in_mol 	= edge.source()

			if not graph.vp.is_food_vtx[in_mol]: # TODO can do more elaborate tests here ...?
				break
		else:
			# Went through -> all edges passed the test
			continue # ...with the outer loop

		break # only get here, if broke out of inner loop

	else:
		# Went through the whole loop without breaking -> is viable
		return True

	return False


# -----------------------------------------------------------------------------
def cycle_stats(graph, cycles, *args, modes: list, enabled: bool=True, **kwargs):
	''' Compute statistics on the given cycles'''

	stats = {}

	if not enabled:
		return stats

	for mode in modes:
		if mode not in STAT_MODES:
			log.warning("Mode %s not available, choose from %s. Skipping.",
			            mode, STAT_MODES)
			continue

		mode_kwargs = kwargs.get('mode_kwargs', {}).get("_"+mode, {})

		# Call evaluation method with these kwargs and save results
		stats[mode] = globals()["_"+mode+"_stats"](graph, cycles, **mode_kwargs)

	return stats

def _length_stats(graph, cycles):
	''' Returns a list of the cycle lenghts.'''
	return [len(cyc) for cyc in cycles]

def _num_cat_edges_stats(graph, cycles):
	''' Returns a list of the number of catalytic edges in each cycle'''
	return [num_catalytic_edges(graph, cyc) for cyc in cycles]
